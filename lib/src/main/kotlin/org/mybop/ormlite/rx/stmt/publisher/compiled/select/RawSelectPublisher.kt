package org.mybop.ormlite.rx.stmt.publisher.compiled.select

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.field.FieldType
import com.j256.ormlite.field.SqlType
import com.j256.ormlite.stmt.GenericRowMapper
import com.j256.ormlite.stmt.StatementBuilder
import com.j256.ormlite.support.CompiledStatement
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import org.mybop.ormlite.rx.stmt.publisher.compiled.CompiledStatementPublisher
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription

/**
 * Publisher that executes the provided raw query and publishes the results mapped as object to the susbscriber.
 *
 * Connection with the database will only be initiated when the publisher subscribes.
 * It will be automatically closed when all the results are pushed or on unsubscription.
 *
 * @param R The class that the result row will be mapped to.
 * @param connectionSource An open [ConnectionSource] to the database.
 * @param tableName Name of the table from which we will read.
 * @param query Raw statement to be executed
 * @param arguments Optional array of string that will be used to replace ? in the query. The number of element in
 * arguments must match the number of ?.
 * @param rowMapper [GenericRowMapper] that will convert the rows returned by the database into the object that will be emitted.
 * @param maxRows Maximum number of rows to read from the results. null to read all.
 */
class RawSelectPublisher<R>(
        private val connectionSource: ConnectionSource,
        tableName: String,
        private val query: String,
        private val arguments: Array<String>,
        private val rowMapper: GenericRowMapper<R>,
        private val objectCache: ObjectCache? = null,
        private val maxRows: Int? = null
) : CompiledStatementPublisher<R>(connectionSource, tableName, true) {

    override fun getCompiledStatement(databaseConnection: DatabaseConnection): CompiledStatement {
        val compiledStatement = databaseConnection.compileStatement(query, StatementBuilder.StatementType.SELECT, arrayOf<FieldType>(),
                DatabaseConnection.DEFAULT_RESULT_FLAGS, false)
        arguments.forEachIndexed { index, it ->
            compiledStatement.setObject(index, it, SqlType.STRING)
        }
        if (maxRows != null) {
            compiledStatement.setMaxRows(maxRows)
        }
        return compiledStatement
    }

    override fun createSubscription(
            subscriber: Subscriber<in R>,
            databaseConnection: DatabaseConnection,
            compiledStatement: CompiledStatement): Subscription
            = CompiledSelectSubscription(
            subscriber, connectionSource, databaseConnection, compiledStatement,
            compiledStatement.runQuery(objectCache),
            rowMapper)
}
