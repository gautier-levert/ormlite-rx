package org.mybop.ormlite.rx.misc

import com.j256.ormlite.support.ConnectionSource
import io.reactivex.Completable
import java.sql.SQLException

/**
 * Provides basic transaction support for a [ConnectionSource].
 *
 * **NOTE:** For transactions to work, the database being used must support the functionality.
 *
 * @see com.j256.ormlite.misc.TransactionManager
 */
class RxTransactionManager(private val connectionSource: ConnectionSource, private val tableName: String? = null) {

    /**
     * Wrap the [Completable] inside of a transaction.
     * If the [Completable] complete successfully then the transaction is committed.
     * If the [Completable] throws an exception then the transaction is rolled back
     * and a [SQLException] is sent by this [Completable].
     *
     * **WARNING:** it is up to you to properly manage threads and schedulers of your [Completable]. There could be side
     * effects of using multiple threads. We recommend to use only one thread for all the completable execution.
     *
     * @param completable
     *            Completable to be wrapped inside of the transaction.
     * @return Another completable that will open a transaction on subscribe and commit or rollback at the end.
     */
    fun callInTransaction(completable: Completable): Completable {
        return TransactionCompletable(connectionSource, tableName, completable)
    }
}
