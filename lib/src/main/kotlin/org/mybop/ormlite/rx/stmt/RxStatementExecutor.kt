package org.mybop.ormlite.rx.stmt

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.db.DatabaseType
import com.j256.ormlite.stmt.PreparedDelete
import com.j256.ormlite.stmt.PreparedStmt
import com.j256.ormlite.stmt.PreparedUpdate
import com.j256.ormlite.stmt.StatementExecutor
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.TableInfo
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import org.mybop.ormlite.rx.stmt.publisher.PublisherBuilder
import org.mybop.ormlite.rx.stmt.publisher.exception.NoRowModifiedException

/**
 * Executes SQL statements for a particular table in a particular database. Basically a call through to various mapped
 * statement methods.
 *
 * @param <T>
 *            The class that the code will be operating on.
 * @param <ID>
 *            The class of the ID column associated with the class. The T class does not require an ID field. The class
 *            needs an ID parameter however so you can use Void or Object to satisfy the compiler.
 * @see StatementExecutor
 */
class RxStatementExecutor<T, ID> internal constructor(
        private val mappedStatements: CommonlyUsedMappedStatements<T, ID>,
        private val publisherBuilder: PublisherBuilder<T, ID>
) {

    constructor(
            databaseType: DatabaseType,
            tableInfo: TableInfo<T, ID>
    ) : this(
            CommonlyUsedMappedStatements(databaseType, tableInfo),
            PublisherBuilder(databaseType, tableInfo)
    )

    /**
     * Returns a [Flowable] of all of the data in the table that matches the [PreparedStmt].
     */
    fun query(connectionSource: ConnectionSource, preparedStmt: PreparedStmt<T>, objectCache: ObjectCache?): Flowable<T>
            = Flowable.fromPublisher(publisherBuilder.select(connectionSource, preparedStmt, objectCache))

    /**
     * Returns a [Maybe] emitting the first row in the table that matches the [PreparedStmt] or completing immediately if none.
     */
    fun queryForFirst(connectionSource: ConnectionSource, preparedStmt: PreparedStmt<T>, objectCache: ObjectCache?): Maybe<T>
            = Flowable.fromPublisher(publisherBuilder.select(connectionSource, preparedStmt, objectCache, 1))
            .firstElement()

    /**
     * Returns a [Single] emitting the number of rows in the table that matches the [PreparedStmt].
     */
    fun queryForCount(connectionSource: ConnectionSource, preparedStmt: PreparedStmt<T>): Single<Long>
            = Flowable.fromPublisher(publisherBuilder.count(connectionSource, preparedStmt))
            .singleOrError()

    /**
     * Returns a [Completable] completing if the data has been inserted in database otherwise emits an error.
     */
    fun create(connectionSource: ConnectionSource, data: T, objectCache: ObjectCache?): Completable
            = mappedStatements.mappedCreate
            .flatMapPublisher { publisherBuilder.create(connectionSource, it, data, objectCache) }
            .flatMapCompletable { if (it == 0) Completable.error(NoRowModifiedException()) else Completable.complete() }

    /**
     * Returns a [Completable] completing if the data has been updated in database otherwise emits an error.
     */
    fun update(connectionSource: ConnectionSource, data: T, objectCache: ObjectCache?): Completable
            = mappedStatements.mappedUpdate
            .flatMapPublisher { publisherBuilder.update(connectionSource, it, data, objectCache) }
            .flatMapCompletable { if (it == 0) Completable.error(NoRowModifiedException()) else Completable.complete() }

    /**
     * Returns a [Completable] completing if the identifier of the data has been updated in database otherwise emits
     * an error.
     */
    fun updateId(connectionSource: ConnectionSource, data: T, newId: ID, objectCache: ObjectCache?): Completable
            = mappedStatements.mappedUpdateId
            .flatMapPublisher { publisherBuilder.updateId(connectionSource, it, data, newId, objectCache) }
            .flatMapCompletable { if (it == 0) Completable.error(NoRowModifiedException()) else Completable.complete() }

    /**
     * Returns a [Single] emitting the number of rows modified in database.
     */
    fun update(connectionSource: ConnectionSource, preparedUpdate: PreparedUpdate<T>): Single<Int>
            = Single.fromPublisher(publisherBuilder.preparedUpdate(connectionSource, preparedUpdate))

    /**
     * Return a [Single] emitting the data parameter with fields value refreshed from database. The id field of the data
     * parameter must be set for the refresh operation to succeed.
     */
    fun refresh(connectionSource: ConnectionSource, data: T, objectCache: ObjectCache?): Single<T>
            = mappedStatements.mappedRefresh
            .flatMapPublisher { publisherBuilder.refresh(connectionSource, it, data, objectCache) }
            .singleOrError()

    /**
     * Returns a [Completable] completing if the data has been deleted from database otherwise emits an error.
     */
    fun delete(connectionSource: ConnectionSource, data: T, objectCache: ObjectCache?): Completable
            = mappedStatements.mappedDelete
            .flatMapPublisher { publisherBuilder.delete(connectionSource, it, data, objectCache) }
            .ignoreElements()

    /**
     * Returns a [Completable] completing if the data has been deleted from database otherwise emits an error.
     */
    fun deleteById(connectionSource: ConnectionSource, id: ID, objectCache: ObjectCache?): Completable
            = mappedStatements.mappedDelete
            .flatMapPublisher { publisherBuilder.deleteById(connectionSource, it, id, objectCache) }
            .ignoreElements()

    /**
     * Returns a [Completable] completing if all the data has been deleted from database otherwise emits an error.
     */
    fun deleteObjects(connectionSource: ConnectionSource, dataCollection: Collection<T>, objectCache: ObjectCache?): Completable
            = Single.fromPublisher(publisherBuilder.deleteObjects(connectionSource, dataCollection, objectCache))
            .ignoreElement()

    /**
     * Returns a [Completable] completing if all the data has been deleted from database otherwise emits an error.
     */
    fun deleteIds(connectionSource: ConnectionSource, idCollection: Collection<ID>, objectCache: ObjectCache?): Completable
            = Single.fromPublisher(publisherBuilder.deleteIds(connectionSource, idCollection, objectCache))
            .ignoreElement()

    /**
     * Returns a [Single] emitting the number of rows deleted from database.
     */
    fun delete(connectionSource: ConnectionSource, preparedDelete: PreparedDelete<T>): Single<Int>
            = Single.fromPublisher(publisherBuilder.preparedDelete(connectionSource, preparedDelete))
}
