package org.mybop.ormlite.rx.stmt.where

import com.j256.ormlite.stmt.PreparedDelete
import com.j256.ormlite.stmt.Where
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import io.reactivex.Completable
import io.reactivex.Single
import org.assertj.core.api.Assertions
import org.junit.Test
import org.mybop.ormlite.rx.model.TestModel
import org.mybop.ormlite.rx.stmt.RxDeleteBuilder

class RxDeleteWhereTest {

    @Test
    fun prepare() {
        val where = mock<Where<TestModel, Long>>()

        val preparedStatement = mock<PreparedDelete<TestModel>>()
        val rxStatementBuilder = mock<RxDeleteBuilder<TestModel, Long>> {
            on { prepare() } doReturn Single.just(preparedStatement)
        }

        val rxWhere = RxDeleteWhere(where, rxStatementBuilder)

        rxWhere.prepare().subscribe { rxPreparedStatement ->
            Assertions.assertThat(rxPreparedStatement).isEqualTo(preparedStatement)
        }
    }

    @Test
    fun delete() {
        val where = mock<Where<TestModel, Long>>()

        val rxStatementBuilder = mock<RxDeleteBuilder<TestModel, Long>> {
            on { delete() } doReturn Completable.complete()
        }

        val rxWhere = RxDeleteWhere(where, rxStatementBuilder)

        var completed = false
        rxWhere.delete().subscribe {
            completed = true
        }

        Assertions.assertThat(completed).isTrue()
    }
}
