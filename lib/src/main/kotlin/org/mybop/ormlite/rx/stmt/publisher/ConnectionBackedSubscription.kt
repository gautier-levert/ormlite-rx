package org.mybop.ormlite.rx.stmt.publisher

import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import io.reactivex.internal.subscriptions.SubscriptionHelper
import org.mybop.ormlite.rx.support.releaseConnectionQuietly
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription

/**
 * A [Subscription] that maintains a connection with the database until it has completed or has been canceled.
 *
 * @param subscriber Subscriber to emit the error to.
 * @param connectionSource [ConnectionSource] that have provided the connection.
 * @param connection An open [DatabaseConnection] on which we will read from/write to.
 */
abstract class ConnectionBackedSubscription<T>(
        private val subscriber: Subscriber<in T>,
        private val connectionSource: ConnectionSource,
        private val connection: DatabaseConnection
) : Subscription {

    private var internalClosed: Boolean = false
    private var internalCanceled: Boolean = false

    /**
     * Return true if the underlying database connection has been closed.
     * @return true if the underlying database connection has been closed.
     */
    val closed: Boolean
        get() = internalClosed

    /**
     * Return true if [cancel] has been called on this subscription.
     * @return true if this subscription has been canceled.
     */
    val canceled: Boolean
        get() = internalCanceled

    /**
     * Implements this method instead of [request]. The connection will automatically be closed in case of exception.
     * It will not be called if the connection is already closed or if the subscription has been canceled.
     */
    protected abstract fun doRequest(n: Long)

    /**
     * Implements this method to release your resources at the same time that the connection source is closed.
     */
    protected abstract fun close()

    /**
     * Close the underlying database connection.
     */
    internal fun closeConnectionQuietly() {
        if (!closed) {
            internalClosed = true
            close()
            connectionSource.releaseConnectionQuietly(connection)
        }
    }

    override fun request(n: Long) {
        if (SubscriptionHelper.validate(n)) {
            if (internalCanceled) {
                return
            }
            if (internalClosed) {
                subscriber.onError(IllegalStateException("cannot request from a closed database connection."))
                return
            }
            try {
                doRequest(n)
            } catch (e: Exception) {
                closeConnectionQuietly()
                subscriber.onError(e)
            }
        }
    }

    override fun cancel() {
        if (!internalCanceled) {
            internalCanceled = true
            closeConnectionQuietly()
        }
    }
}
