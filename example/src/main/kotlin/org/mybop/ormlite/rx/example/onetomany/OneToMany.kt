package org.mybop.ormlite.rx.example.onetomany

import com.j256.ormlite.jdbc.JdbcConnectionSource
import com.j256.ormlite.table.TableUtils
import org.mybop.ormlite.rx.dao.RxDaoManager

const val DATABASE_URL = "jdbc:h2:mem:account"

fun main(vararg args: String) {
    JdbcConnectionSource(DATABASE_URL).use { connectionSource ->
        val accountDao = RxDaoManager.createDao<Account, Int>(connectionSource, Account::class.java)
        val messageDao = RxDaoManager.createDao<Message, Long>(connectionSource, Message::class.java)

        TableUtils.createTable(connectionSource, Account::class.java)
        TableUtils.createTable(connectionSource, Message::class.java)

        // create an instance of Account
        val account = Account(name = "Jim Coakley")

        accountDao.create(account)
                .flatMap {
                    println(it)
                    messageDao.create(Message(content = "I was here", creator = it))
                }
                .subscribe { message ->
                    println(message)
                }

        accountDao.refresh(account)
                .subscribe { it ->
                    println(it)
                    it.messages?.add(Message(content = "I was here too"))
                }

        messageDao.queryForAll()
                .subscribe { message ->
                    println(message)
                }
    }
}
