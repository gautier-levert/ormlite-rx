package org.mybop.ormlite.rx.dao

import com.j256.ormlite.dao.BaseDaoImpl
import com.j256.ormlite.dao.Dao
import com.j256.ormlite.dao.DaoManager
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.DatabaseTable
import com.j256.ormlite.table.DatabaseTableConfig
import java.sql.SQLException

/**
 * Class which caches created DAOs. Sometimes internal DAOs are used to support such features as auto-refreshing of
 * foreign fields or collections of sub-objects. Since instantiation of the DAO is a bit expensive, this class is used
 * in an attempt to only create a DAO once for each class.
 *
 * <p>
 * <b>NOTE:</b> To use this cache, you should make sure you've added a [CustomRxDao] value to the
 * annotation to the top of your class.
 * </p>
 *
 * @see DaoManager
 */
object RxDaoManager {

    private val daoMap = mutableMapOf<Dao<*, *>, RxDao<*, *>>()

    /**
     * Helper method to create a DAO object without having to define a class. This checks to see if the DAO has already
     * been created. If not then it is a call through to [BaseRxDaoImpl.create].
     */
    @Throws(SQLException::class)
    @Suppress("unchecked_cast")
    fun <T, ID> createDao(connectionSource: ConnectionSource, clazz: Class<T>): RxDao<T, ID> = synchronized(this) {
        val dao = DaoManager.createDao(connectionSource, clazz)
        return createRxDao(dao as Dao<T, ID>)
    }

    /**
     * Helper method to lookup a DAO if it has already been associated with the class. Otherwise this returns null.
     */
    @Suppress("unchecked_cast")
    fun <T, ID> lookupDao(connectionSource: ConnectionSource, clazz: Class<T>): RxDao<T, ID>? = synchronized(this) {
        val dao = DaoManager.lookupDao(connectionSource, clazz)
        return dao?.let { daoMap[it] as RxDao<T, ID>? }
    }

    /**
     * Helper method to create a DAO object without having to define a class. This checks to see if the DAO has already
     * been created. If not then it is a call through to
     * [BaseRxDaoImpl.create].
     */
    @Throws(SQLException::class)
    @Suppress("unchecked_cast")
    fun <T, ID> createDao(connectionSource: ConnectionSource, tableConfig: DatabaseTableConfig<T>): RxDao<T, ID> = synchronized(this) {
        val dao = DaoManager.createDao(connectionSource, tableConfig)
        return createRxDao(dao as Dao<T, ID>)
    }

    /**
     * Helper method to lookup a DAO if it has already been associated with the table-config. Otherwise this returns
     * null.
     */
    @Suppress("unchecked_cast")
    fun <T, ID> lookupDao(connectionSource: ConnectionSource, tableConfig: DatabaseTableConfig<T>): RxDao<T, ID>? = synchronized(this) {
        val dao = DaoManager.lookupDao(connectionSource, tableConfig)
        return dao?.let { daoMap[it] as RxDao<T, ID>? }
    }

    /**
     * Register the DAO with the cache. This will allow folks to build a DAO externally and then register so it can be
     * used internally as necessary.
     *
     * **NOTE:** By default this registers the DAO to be associated with the class that it uses. If you need to
     * register multiple rxDao's that use different [DatabaseTableConfig]s then you should use
     * [registerDaoWithTableConfig].
     *
     * **NOTE:** You should maybe use the [DatabaseTable.daoClass] and have the DaoManager construct the DAO
     * if possible.
     */
    fun registerDao(connectionSource: ConnectionSource, rxDao: BaseRxDaoImpl<*, *>) = synchronized(this) {
        DaoManager.registerDao(connectionSource, rxDao.dao)
        daoMap[rxDao.dao] = rxDao
    }

    /**
     * Remove a DAO from the cache. This is necessary if we've registered it already but it throws an exception during
     * configuration.
     */
    fun unregisterDao(connectionSource: ConnectionSource, rxDao: BaseRxDaoImpl<*, *>) = synchronized(this) {
        DaoManager.unregisterDao(connectionSource, rxDao.dao)
        daoMap.remove(rxDao.dao)
    }

    /**
     * Same as [registerDao] but this allows you to register it just with its
     * [DatabaseTableConfig]. This allows multiple versions of the DAO to be configured if necessary.
     */
    fun registerDaoWithTableConfig(connectionSource: ConnectionSource, rxDao: BaseRxDaoImpl<*, *>) = synchronized(this) {
        DaoManager.registerDaoWithTableConfig(connectionSource, rxDao.dao)
        daoMap[rxDao.dao] = rxDao
    }

    /**
     * Clear out all of internal caches.
     */
    fun clearCache() = synchronized(this) {
        DaoManager.clearCache()
        daoMap.clear()
    }

    /**
     * Clear out our DAO caches.
     */
    fun clearDaoCache() = synchronized(this) {
        DaoManager.clearDaoCache()
        daoMap.clear()
    }

    @Suppress("unchecked_cast")
    private fun <T, ID> createRxDao(dao: Dao<T, ID>): RxDao<T, ID> = synchronized(this) {
        if (daoMap.contains(dao)) {
            return daoMap[dao] as RxDao<T, ID>
        } else {
            if (dao !is BaseDaoImpl<T, ID>) {
                throw IllegalArgumentException("Dao that does'nt extend BaseDaoImpl are not supported by OrmLite-Rx")
            }

            val rxDao = dao.dataClass.annotations
                    .firstOrNull { it is CustomRxDao }
                    ?.let {
                        (it as CustomRxDao).rxDaoClass
                    }
                    ?.let {
                        it.constructors
                                .filter {
                                    it.parameters.size == 1
                                }.firstOrNull {
                                    it.parameters[0].type.classifier == BaseDaoImpl::class
                                }
                                ?.call(dao) as RxDao<T, ID>?
                                ?: throw IllegalStateException("the class ${it.simpleName} must have a constructor with a single parameter of type BaseDaoImpl")
                    }
                    ?: BaseRxDaoImpl(dao)

            daoMap[dao] = rxDao
            return rxDao
        }
    }
}
