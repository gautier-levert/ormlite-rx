package org.mybop.ormlite.rx.stmt.publisher.compiled.execute

import com.j256.ormlite.field.SqlType
import com.j256.ormlite.stmt.StatementBuilder
import com.j256.ormlite.support.CompiledStatement
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.Test
import org.reactivestreams.Subscriber

class RawExecutePublisherTest {

    @Test
    fun subscribe_execute() {
        val query = "UPDATE test SET value = ? WHERE id = ?"
        val arguments = arrayOf("value", "testId")
        val compiledStatement = mock<CompiledStatement>()
        val connection = mock<DatabaseConnection> {
            on {
                compileStatement(query, StatementBuilder.StatementType.EXECUTE, emptyArray(),
                        DatabaseConnection.DEFAULT_RESULT_FLAGS, false)
            } doReturn compiledStatement
        }
        val connectionSource = mock<ConnectionSource> {
            on { getReadWriteConnection(null) } doReturn connection
        }
        val subscriber = mock<Subscriber<in Int>>()

        val publisher = RawExecutePublisher(
                connectionSource,
                query,
                arguments,
                false
        )

        publisher.subscribe(subscriber)

        verify(compiledStatement).setObject(0, arguments[0], SqlType.STRING)
        verify(compiledStatement).setObject(1, arguments[1], SqlType.STRING)

        verify(subscriber).onSubscribe(any<CompiledExecuteSubscription>())
    }

    @Test
    fun subscribe_update() {
        val query = "UPDATE test SET value = ? WHERE id = ?"
        val arguments = arrayOf("value", "testId")
        val compiledStatement = mock<CompiledStatement>()
        val connection = mock<DatabaseConnection> {
            on {
                compileStatement(query, StatementBuilder.StatementType.UPDATE, emptyArray(),
                        DatabaseConnection.DEFAULT_RESULT_FLAGS, false)
            } doReturn compiledStatement
        }
        val connectionSource = mock<ConnectionSource> {
            on { getReadWriteConnection(null) } doReturn connection
        }
        val subscriber = mock<Subscriber<in Int>>()

        val publisher = RawExecutePublisher(
                connectionSource,
                query,
                arguments,
                true
        )

        publisher.subscribe(subscriber)

        verify(compiledStatement).setObject(0, arguments[0], SqlType.STRING)
        verify(compiledStatement).setObject(1, arguments[1], SqlType.STRING)

        verify(subscriber).onSubscribe(any<CompiledExecuteSubscription>())
    }
}
