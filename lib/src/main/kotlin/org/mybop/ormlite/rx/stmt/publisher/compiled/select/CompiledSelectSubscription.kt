package org.mybop.ormlite.rx.stmt.publisher.compiled.select

import com.j256.ormlite.stmt.GenericRowMapper
import com.j256.ormlite.support.CompiledStatement
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import com.j256.ormlite.support.DatabaseResults
import org.mybop.ormlite.rx.stmt.publisher.ConnectionBackedSubscription
import org.reactivestreams.Subscriber
import java.sql.SQLException

/**
 * Subscription that pushes mapped results from database as the publisher requests them.
 * The connection is automatically closed when the publisher has consumed all the results.
 *
 * @param T The class that the code will be operating on.
 * @param subscriber [Subscriber] to emit the mapped results to.
 * @param connectionSource [ConnectionSource] that have provided the connection.
 * @param connection [DatabaseConnection] on which the mapped statement will be executed.
 * @param statement Statement that have been executed to get the database results.
 * @param databaseResults [DatabaseResults] that will be emitted to the subscriber as it requests them.
 * @param rowMapper [GenericRowMapper] that will convert the rows returned by the database into the object that will be emitted.
 */
internal class CompiledSelectSubscription<T>(
        private val subscriber: Subscriber<in T>,
        connectionSource: ConnectionSource,
        connection: DatabaseConnection,
        private val statement: CompiledStatement,
        private val databaseResults: DatabaseResults,
        private val rowMapper: GenericRowMapper<T>
) : ConnectionBackedSubscription<T>(subscriber, connectionSource, connection) {

    private var first = true
    private var fullyConsumed = false

    override fun doRequest(n: Long) {
        var nbRowToEmit = n
        var lastRow: T?
        do {
            lastRow = nextRowAsObject()
            if (!canceled && lastRow != null) {
                subscriber.onNext(lastRow)
            }
        } while (!canceled && --nbRowToEmit > 0 && lastRow != null)

        if (!canceled && lastRow == null) {
            closeConnectionQuietly()
            subscriber.onComplete()
        }
    }

    /**
     * Move to the next row in the results. Close automatically the connection when the database results has been fully consumed.
     *
     * @return false if there is no more row to proceed, true otherwise.
     * @throws SQLException if an error occurs when communicating with the database.
     * @throws IllegalStateException if database connection has already been closed.
     */
    @Throws(SQLException::class, IllegalStateException::class)
    internal fun moveToNextRow(): Boolean {
        if (fullyConsumed) {
            return false
        }
        if (closed) {
            throw IllegalStateException("database connection was close before reaching the end of the result set.")
        }
        if (first) {
            first = false
            fullyConsumed = !databaseResults.first()
        } else {
            fullyConsumed = !databaseResults.next()
        }
        if (fullyConsumed) {
            closeConnectionQuietly()
        }
        return !fullyConsumed
    }

    /**
     * Returns the next row of the result set mapped into an object.
     *
     * @return the next row of the result set or null if there is no more row to consume.
     * @throws SQLException if an error occurs when communicating with the database.
     * @throws IllegalStateException if database connection has already been closed or the results have been completely consumed.
     */
    @Throws(SQLException::class, IllegalStateException::class)
    internal fun nextRowAsObject(): T? {
        return if (moveToNextRow()) {
            rowMapper.mapRow(databaseResults)
        } else {
            null
        }
    }

    override fun close() {
        statement.closeQuietly()
    }
}
