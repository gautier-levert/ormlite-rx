package org.mybop.ormlite.rx.example.customdao

import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import org.mybop.ormlite.rx.dao.CustomRxDao

@DatabaseTable(tableName = "account")
@CustomRxDao(AccountDao::class)
data class Account(
        @DatabaseField(generatedId = true)
        val id: Int = 0,

        @DatabaseField(columnName = NAME_COLUMN_NAME, canBeNull = false)
        var name: String = "",

        @DatabaseField(columnName = PASSWORD_COLUMN_NAME)
        var password: String = "",

        @DatabaseField(columnName = ACTIVE_COLUMN_NAME, index = true)
        var active: Boolean = true
) {
    companion object {
        const val NAME_COLUMN_NAME = "name"
        const val PASSWORD_COLUMN_NAME = "password"
        const val ACTIVE_COLUMN_NAME = "active"
    }
}
