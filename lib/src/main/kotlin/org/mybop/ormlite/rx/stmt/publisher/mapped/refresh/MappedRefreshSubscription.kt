package org.mybop.ormlite.rx.stmt.publisher.mapped.refresh

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.stmt.mapped.MappedRefresh
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import org.mybop.ormlite.rx.stmt.publisher.exception.NotRefreshedException
import org.mybop.ormlite.rx.stmt.publisher.mapped.MappedStatementSubscription
import org.reactivestreams.Subscriber

/**
 * A subscription that will refresh the data from the database and emits them to the subscriber.
 *
 * The data parameter will be modified before being emitted to the subscriber. This is done to avoid cloning
 * the entire data item.
 *
 * @param T The class that the code will be operating on.
 * @param ID The class of the ID column associated with the class.
 * @param subscriber [Subscriber] to emit the number of rows to.
 * @param connectionSource [ConnectionSource] that have provided the connection.
 * @param connection [DatabaseConnection] on which the mapped statement will be executed.
 * @param mappedStatement Refresh statement that will be executed when a publisher subscribes and requests.
 * @param data Data to refresh from the database. ID field must be set.
 * @param objectCache Optional [ObjectCache].
 *
 * @see MappedStatementSubscription
 */
class MappedRefreshSubscription<T, ID>(
        subscriber: Subscriber<in T>,
        connectionSource: ConnectionSource,
        connection: DatabaseConnection,
        mappedStatement: MappedRefresh<T, ID>,
        private val data: T,
        objectCache: ObjectCache?
) : MappedStatementSubscription<T, ID, T, MappedRefresh<T, ID>>(
        subscriber, connectionSource, connection, mappedStatement, objectCache
) {
    override fun execute(
            mappedStatement: MappedRefresh<T, ID>,
            connection: DatabaseConnection,
            objectCache: ObjectCache?): T {
        val result = mappedStatement.executeRefresh(connection, data, objectCache)
        if (result == 0) {
            throw NotRefreshedException()
        }
        return data
    }
}
