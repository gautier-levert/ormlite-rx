package org.mybop.ormlite.rx.mapper

import com.j256.ormlite.stmt.GenericRowMapper
import com.j256.ormlite.support.DatabaseResults

/**
 * A simple [GenericRowMapper] that retrieves the first column of the row as long and returns it.
 */
object LongRowMapper : GenericRowMapper<Long> {
    override fun mapRow(results: DatabaseResults): Long = results.getLong(0)
}
