package org.mybop.ormlite.rx.stmt

import com.j256.ormlite.dao.Dao
import com.j256.ormlite.field.ForeignCollectionField
import com.j256.ormlite.stmt.ArgumentHolder
import com.j256.ormlite.stmt.GenericRowMapper
import com.j256.ormlite.stmt.PreparedQuery
import com.j256.ormlite.stmt.QueryBuilder
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import org.mybop.ormlite.rx.dao.BaseRxDaoImpl
import org.mybop.ormlite.rx.mapper.StringArrayRowMapper
import org.mybop.ormlite.rx.stmt.where.RxQueryWhere
import java.sql.SQLException

class RxQueryBuilder<T, ID>(
        internal val queryBuilder: QueryBuilder<T, ID>,
        private val rxDao: BaseRxDaoImpl<T, ID>
) : RxStatementBuilder<T, ID, RxQueryWhere<T, ID>>(queryBuilder) {

    override fun where(): RxQueryWhere<T, ID> = RxQueryWhere(queryBuilder.where(), this)

    /**
     * Build and return a prepared query that can be used by [Dao.query] or
     * [Dao.iterator] methods. If you change the where or make other calls you will need to re-call
     * this method to re-prepare the statement for execution.
     */
    override fun prepare(): Single<PreparedQuery<T>> = Single.fromCallable { queryBuilder.prepare() }

    /**
     * Add columns to be returned by the SELECT query. If no columns are selected then all columns are returned by
     * default. For classes with id columns, the id column is added to the select list auto-magically. This can be called
     * multiple times to add more columns to select.
     *
     * **WARNING:** If you specify any columns to return, then any foreign-collection fields will be returned as null
     * *unless* their [ForeignCollectionField.columnName] is also in the list.
     */
    fun selectColumns(vararg columns: String): RxQueryBuilder<T, ID> {
        queryBuilder.selectColumns(*columns)
        return this
    }

    /**
     * Same as [selectColumns] except the columns are specified as an [Iterable] -- probably will be a
     * [Collection]. This can be called multiple times to add more columns to select.
     */
    fun selectColumns(columns: Iterable<String>): RxQueryBuilder<T, ID> {
        queryBuilder.selectColumns(columns)
        return this
    }

    /**
     * Add raw columns or aggregate functions (COUNT, MAX, ...) to the query. This will turn the query into something
     * only suitable for the [Dao.queryRaw] type of statement. This can be called multiple
     * times to add more columns to select.
     */
    fun selectRaw(vararg columns: String): RxQueryBuilder<T, ID> {
        queryBuilder.selectRaw(*columns)
        return this
    }

    /**
     * Add "GROUP BY" clause to the SQL query statement. This can be called multiple times to add additional "GROUP BY"
     * clauses.
     *
     * NOTE: Use of this means that the resulting objects may not have a valid ID column value so cannot be deleted or
     * updated.
     */
    fun groupBy(columnName: String): RxQueryBuilder<T, ID> {
        queryBuilder.groupBy(columnName)
        return this
    }

    /**
     * Add a raw SQL "GROUP BY" clause to the SQL query statement. This should not include the "GROUP BY".
     */
    fun groupByRaw(rawSql: String): RxQueryBuilder<T, ID> {
        queryBuilder.groupByRaw(rawSql)
        return this
    }

    /**
     * Add "ORDER BY" clause to the SQL query statement. This can be called multiple times to add additional "ORDER BY"
     * clauses. Ones earlier are applied first.
     */
    fun orderBy(columnName: String, ascending: Boolean = true): RxQueryBuilder<T, ID> {
        queryBuilder.orderBy(columnName, ascending)
        return this
    }

    /**
     * Add raw SQL "ORDER BY" clause to the SQL query statement.
     *
     * @param rawSql
     *            The raw SQL order by clause. This should not include the "ORDER BY".
     */
    fun orderByRaw(rawSql: String): RxQueryBuilder<T, ID> {
        queryBuilder.orderByRaw(rawSql)
        return this
    }

    /**
     * Add raw SQL "ORDER BY" clause to the SQL query statement.

     * @param rawSql
     *            The raw SQL order by clause. This should not include the "ORDER BY".
     *
     * @param args
     *            Optional arguments that correspond to any ? specified in the rawSql. Each of the arguments must have
     *            the sql-type set.
     */
    fun orderByRaw(rawSql: String, vararg args: ArgumentHolder): RxQueryBuilder<T, ID> {
        queryBuilder.orderByRaw(rawSql, *args)
        return this
    }

    /**
     * Add "DISTINCT" clause to the SQL query statement.
     *
     * **NOTE:** Use of this means that the resulting objects may not have a valid ID column value so cannot be deleted or
     * updated.
     */
    fun distinct(): RxQueryBuilder<T, ID> {
        queryBuilder.distinct()
        return this
    }

    /**
     * Limit the output to maxRows maximum number of rows. Set to null for no limit (the default).
     */
    fun limit(maxRows: Long?): RxQueryBuilder<T, ID> {
        queryBuilder.limit(maxRows)
        return this
    }

    /**
     * Start the output at this row number. Set to null for no offset (the default). If you are paging through a table,
     * you should consider using the [Dao.iterator] method instead which handles paging with a database cursor.
     * Otherwise, if you are paging you probably want to specify a [orderBy].
     *
     * **NOTE:** This is not supported for all databases. Also, for some databases, the limit _must_ also be
     * specified since the offset is an argument of the limit.
     */
    @Throws(SQLException::class)
    fun offset(startRow: Long?): RxQueryBuilder<T, ID> {
        queryBuilder.offset(startRow)
        return this
    }

    /**
     * Set whether or not we should only return the count of the results. This query can then be used by
     * [Dao.countOf].
     * To get the count-of directly, use [countOf].
     */
    fun setCountOf(countOf: Boolean): RxQueryBuilder<T, ID> {
        setCountOf(if (countOf) "*" else null)
        return this
    }

    /**
     * Set the field that we are counting from the database. For example, you can
     * `qb.setCountOf("DISTINCT(fieldName)")`. This query can then be used by [Dao.countOf].
     * To get the count-of directly, use [countOf].
     */
    fun setCountOf(countOfQuery: String?): RxQueryBuilder<T, ID> {
        queryBuilder.setCountOf(countOfQuery)
        return this
    }

    /**
     * Add raw SQL "HAVING" clause to the SQL query statement. This should not include the "HAVING" string.
     */
    fun having(having: String?): RxQueryBuilder<T, ID> {
        queryBuilder.having(having)
        return this
    }

    /**
     * Join with another query builder. This will add into the SQL something close to " INNER JOIN other-table ...".
     * Either the object associated with the current QueryBuilder or the argument QueryBuilder must have a foreign field
     * of the other one. An exception will be thrown otherwise.
     *
     * **NOTE:** This will do combine the WHERE statement of the two query builders with a SQL "AND". See
     * [joinOr].
     */
    @Throws(SQLException::class)
    fun join(joinedQueryBuilder: RxQueryBuilder<*, *>): RxQueryBuilder<T, ID> {
        queryBuilder.join(joinedQueryBuilder.queryBuilder)
        return this
    }

    /**
     * Like [join] but allows you to specify the join type and the operation used to combine the
     * WHERE statements.
     */
    @Throws(SQLException::class)
    fun join(
            joinedQueryBuilder: RxQueryBuilder<*, *>,
            type: QueryBuilder.JoinType,
            operation: QueryBuilder.JoinWhereOperation
    ): RxQueryBuilder<T, ID> {
        queryBuilder.join(joinedQueryBuilder.queryBuilder, type, operation)
        return this
    }

    /**
     * Like [join] but this combines the WHERE statements of two query builders with a SQL "OR".
     */
    @Throws(SQLException::class)
    fun joinOr(joinedQueryBuilder: RxQueryBuilder<*, *>): RxQueryBuilder<T, ID> {
        queryBuilder.joinOr(joinedQueryBuilder.queryBuilder)
        return this
    }

    /**
     * Similar to [join] but it will use "LEFT JOIN" instead.
     * See: [LEFT JOIN SQL docs](http://www.w3schools.com/sql/sql_join_left.asp)
     *
     * **NOTE:** RIGHT and FULL JOIN SQL commands are not supported because we are only returning objects from the
     * "left" table.
     *
     * **NOTE:** This will do combine the WHERE statement of the two query builders with a SQL "AND". See
     * [leftJoinOr].
     */
    @Throws(SQLException::class)
    fun leftJoin(joinedQueryBuilder: RxQueryBuilder<*, *>): RxQueryBuilder<T, ID> {
        queryBuilder.leftJoin(joinedQueryBuilder.queryBuilder)
        return this
    }

    /**
     * Like [leftJoin] but this combines the WHERE statements of two query builders with a SQL "OR".
     */
    @Throws(SQLException::class)
    fun leftJoinOr(joinedQueryBuilder: RxQueryBuilder<*, *>): RxQueryBuilder<T, ID> {
        queryBuilder.leftJoinOr(joinedQueryBuilder.queryBuilder)
        return this
    }

    /**
     * Similar to [join] but this allows you to link two tables that share a field of the same
     * type. So even if there is _not_ a foreign-object relationship between the tables, you can JOIN them. This will
     * add into the SQL something close to " INNER JOIN other-table ...".
     */
    @Throws(SQLException::class)
    fun join(
            localColumnName: String, joinedColumnName: String,
            joinedQueryBuilder: RxQueryBuilder<*, *>
    ): RxQueryBuilder<T, ID> {
        queryBuilder.join(localColumnName, joinedColumnName, joinedQueryBuilder.queryBuilder)
        return this
    }

    /**
     * Similar to [join] but this allows you to link two tables that
     * share a field of the same type.
     */
    @Throws(SQLException::class)
    fun join(
            localColumnName: String, joinedColumnName: String,
            joinedQueryBuilder: RxQueryBuilder<*, *>,
            type: QueryBuilder.JoinType,
            operation: QueryBuilder.JoinWhereOperation
    ): RxQueryBuilder<T, ID> {
        queryBuilder.join(localColumnName, joinedColumnName, joinedQueryBuilder.queryBuilder, type, operation)
        return this
    }

    /**
     * A short cut to [Dao.query].
     */
    fun query(): Flowable<T>
            = prepare().flatMapPublisher { rxDao.query(it) }

    /**
     * A short cut to [Dao.queryRaw] with a [StringArrayRowMapper]
     */
    fun queryRaw(): Flowable<Array<String>>
            = queryRaw(StringArrayRowMapper)

    /**
     * A short cut to [Dao.queryRaw].
     */
    fun <R> queryRaw(rowMapper: GenericRowMapper<R>): Flowable<R>
            = prepareStatementString().flatMapPublisher { rxDao.queryRaw(it, rowMapper) }

    /**
     * A short cut to [Dao.queryForFirst].
     */
    fun queryForFirst(): Maybe<T>
            = prepare().flatMapMaybe { rxDao.queryForFirst(it) }

    /**
     * A short cut to [queryRawFirst][org.mybop.ormlite.rx.dao.RxRawDao.queryRawFirst] wit a [StringArrayRowMapper]
     */
    fun queryRawFirst(): Maybe<Array<String>>
            = prepareStatementString().flatMapMaybe { rxDao.queryRawFirst(it, StringArrayRowMapper) }

    /**
     * A short cut to [queryRawFirst][org.mybop.ormlite.rx.dao.RxRawDao.queryRawFirst].
     */
    fun <R> queryRawFirst(rowMapper: GenericRowMapper<R>): Maybe<R>
            = prepareStatementString().flatMapMaybe { rxDao.queryRawFirst(it, rowMapper) }

    /**
     * Returns the count of the number of rows in the table. This uses [setCountOf] to true and then
     * calls [Dao.countOf]. It DOES NOT restore the previous count-of value before returning.
     */
    fun countOf(): Single<Long> {
        setCountOf(true)
        return prepare()
                .flatMap { rxDao.countOf(it) }
    }

    /**
     * Returns the count of the number of rows that match a field so you can do
     * `qb.countOf("DISTINCT(fieldName)")`. This uses [setCountOf] and then calls
     * [Dao.countOf]. It DOES NOTE restore the previous count-of value before returning.
     */
    fun countOf(countOfQuery: String): Single<Long> {
        setCountOf(countOfQuery)
        return prepare()
                .flatMap { rxDao.countOf(it) }
    }

    /**
     * Add an alias for this table. In FROM clause, the table will use AS to define the alias, and qualified fields will
     * be qualified using the alias instead of table name.
     *
     * null to remove alias
     */
    fun setAlias(alias: String?): RxQueryBuilder<T, ID> {
        queryBuilder.setAlias(alias)
        return this
    }
}
