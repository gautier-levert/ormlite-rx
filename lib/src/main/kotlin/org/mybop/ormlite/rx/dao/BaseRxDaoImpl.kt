package org.mybop.ormlite.rx.dao

import com.j256.ormlite.dao.BaseDaoImpl
import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.dao.RawRowObjectMapper
import com.j256.ormlite.field.DataType
import com.j256.ormlite.stmt.GenericRowMapper
import com.j256.ormlite.stmt.PreparedDelete
import com.j256.ormlite.stmt.PreparedQuery
import com.j256.ormlite.stmt.PreparedUpdate
import com.j256.ormlite.support.ConnectionSource
import io.reactivex.*
import org.mybop.ormlite.rx.mapper.GenericToObjectMapper
import org.mybop.ormlite.rx.misc.BatchCompletable
import org.mybop.ormlite.rx.stmt.*

open class BaseRxDaoImpl<T, ID> internal constructor(
        private val statementExecutor: RxStatementExecutor<T, ID>,
        private val rawStatementExecutor: RxRawStatementExecutor<T, ID>,
        private val statements: CommonlyUsedStatements<T, ID>,
        internal val dao: BaseDaoImpl<T, ID>
) : RxDao<T, ID>, RxRawDao, ObjectCacheManager {

    constructor(dao: BaseDaoImpl<T, ID>) : this(
            buildStatementExecutor(dao),
            buildRawStatementExecutor(dao),
            buildStatements(dao),
            dao
    )

    internal val connectionSource: ConnectionSource
        get() = dao.connectionSource

    override var objectCache: ObjectCache?
        get() = dao.objectCache
        set(value) {
            dao.objectCache = value
        }

    override fun queryForId(id: ID): Maybe<T>
            = statements.queryForId(id).flatMapMaybe { statementExecutor.queryForFirst(connectionSource, it, objectCache) }

    override fun queryForFirst(preparedQuery: PreparedQuery<T>): Maybe<T>
            = statementExecutor.queryForFirst(connectionSource, preparedQuery, objectCache)

    override fun queryForAll(): Flowable<T>
            = statements.queryForAll.flatMapPublisher { statementExecutor.query(connectionSource, it, objectCache) }

    override fun queryBuilder(): RxQueryBuilder<T, ID> =
            RxQueryBuilder(dao.queryBuilder(), this)

    override fun updateBuilder(): RxUpdateBuilder<T, ID> =
            RxUpdateBuilder(dao.updateBuilder(), this)

    override fun deleteBuilder(): RxDeleteBuilder<T, ID> =
            RxDeleteBuilder(dao.deleteBuilder(), this)

    override fun query(preparedQuery: PreparedQuery<T>): Flowable<T>
            = statementExecutor.query(connectionSource, preparedQuery, objectCache)

    override fun create(data: T): Single<T>
            = statementExecutor.create(connectionSource, data, objectCache)
            .toSingleDefault(data)

    override fun create(dataCollection: Collection<T>): Completable
            = BatchCompletable(
            connectionSource,
            dao.tableInfo.tableName,
            Observable.fromIterable(dataCollection)
                    .flatMapSingle { create(it) }
                    .ignoreElements())


    override fun createIfNotExists(data: T): Single<T>
            = create(data)
            .onErrorResumeNext { refresh(data) }

    override fun createOrUpdate(data: T): Single<T>
            = create(data)
            .onErrorResumeNext { update(data) }
            .map { data }

    override fun update(data: T): Single<T>
            = statementExecutor.update(connectionSource, data, objectCache)
            .toSingleDefault(data)

    override fun updateId(data: T, newId: ID): Single<T>
            = statementExecutor.updateId(connectionSource, data, newId, objectCache)
            .toSingleDefault(data)

    override fun update(preparedUpdate: PreparedUpdate<T>): Completable
            = statementExecutor.update(connectionSource, preparedUpdate)
            .ignoreElement()

    override fun refresh(data: T): Single<T>
            = statementExecutor.refresh(connectionSource, data, objectCache)

    override fun delete(data: T): Completable
            = statementExecutor.delete(connectionSource, data, objectCache)

    override fun delete(dataCollection: Collection<T>): Completable
            = statementExecutor.deleteObjects(connectionSource, dataCollection, objectCache)

    override fun deleteById(id: ID): Completable
            = statementExecutor.deleteById(connectionSource, id, objectCache)

    override fun deleteIds(idsCollection: Collection<ID>): Completable
            = statementExecutor.deleteIds(connectionSource, idsCollection, objectCache)

    override fun delete(preparedDelete: PreparedDelete<T>): Completable
            = statementExecutor.delete(connectionSource, preparedDelete)
            .ignoreElement()

    override fun countOf(): Single<Long>
            = statements.countAll.flatMap { statementExecutor.queryForCount(connectionSource, it) }

    override fun countOf(preparedQuery: PreparedQuery<T>): Single<Long>
            = statementExecutor.queryForCount(connectionSource, preparedQuery)

    override fun <R> queryRaw(query: String, rowMapper: GenericRowMapper<R>, vararg args: String): Flowable<R>
            = rawStatementExecutor.queryRaw(connectionSource, query, arrayOf(*args), rowMapper, objectCache)

    override fun <R> queryRawFirst(query: String, rowMapper: GenericRowMapper<R>, vararg args: String): Maybe<R>
            = rawStatementExecutor.queryRawFirst(connectionSource, query, arrayOf(*args), rowMapper, objectCache)

    override fun <R> queryRaw(query: String, dataTypes: Array<DataType>, rowMapper: RawRowObjectMapper<R>, vararg args: String): Flowable<R>
            = queryRaw(query, GenericToObjectMapper(dataTypes, rowMapper), *args)

    override fun executeRaw(query: String, vararg args: String): Completable
            = rawStatementExecutor.executeRaw(connectionSource, query, arrayOf(*args))

    override fun updateRaw(query: String, vararg args: String): Completable
            = rawStatementExecutor.updateRaw(connectionSource, query, arrayOf(*args))

    companion object {
        internal fun <T, ID> buildStatementExecutor(dao: BaseDaoImpl<T, ID>) = RxStatementExecutor(
                dao.connectionSource.databaseType,
                dao.tableInfo
        )

        internal fun <T, ID> buildRawStatementExecutor(dao: BaseDaoImpl<T, ID>) = RxRawStatementExecutor(
                dao.connectionSource.databaseType,
                dao.tableInfo
        )

        internal fun <T, ID> buildStatements(dao: BaseDaoImpl<T, ID>) = CommonlyUsedStatements(
                dao.connectionSource.databaseType,
                dao.tableInfo,
                dao
        )
    }
}
