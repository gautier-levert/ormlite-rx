package org.mybop.ormlite.rx.stmt.publisher.compiled.execute

import com.j256.ormlite.field.SqlType
import com.j256.ormlite.stmt.StatementBuilder
import com.j256.ormlite.support.CompiledStatement
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import org.mybop.ormlite.rx.stmt.publisher.compiled.CompiledStatementPublisher
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import java.util.concurrent.atomic.AtomicInteger

/**
 * Publisher that execute the provided raw statement and publishes the number of rows modified/deleted.
 *
 * @param connectionSource An open [ConnectionSource] to the database.
 * @param query the raw statement to be executed
 * @param arguments arguments are optional but can be set with strings to expand ? type of SQL
 */
class RawExecutePublisher(
        private val connectionSource: ConnectionSource,
        private val query: String,
        private val arguments: Array<String>,
        private val update: Boolean
) : CompiledStatementPublisher<Int>(connectionSource, null, false) {

    private val atomicSubscriptionCount = AtomicInteger()

    override fun subscribe(subscriber: Subscriber<in Int>) {
        val subscriptionCount = atomicSubscriptionCount.incrementAndGet()
        if (subscriptionCount > 1) {
            throw IllegalStateException("Multiple subscription on create/update/delete is not allowed.")
        }
        super.subscribe(subscriber)
    }

    override fun getCompiledStatement(databaseConnection: DatabaseConnection): CompiledStatement {
        val compiledStatement = databaseConnection.compileStatement(
                query,
                if (update)
                    StatementBuilder.StatementType.UPDATE
                else
                    StatementBuilder.StatementType.EXECUTE,
                emptyArray(),
                DatabaseConnection.DEFAULT_RESULT_FLAGS,
                false)
        arguments.forEachIndexed { index, value ->
            compiledStatement.setObject(index, value, SqlType.STRING)
        }
        return compiledStatement
    }

    override fun createSubscription(subscriber: Subscriber<in Int>,
                                    databaseConnection: DatabaseConnection,
                                    compiledStatement: CompiledStatement): Subscription
            = CompiledExecuteSubscription(subscriber, connectionSource, databaseConnection, compiledStatement, update)
}
