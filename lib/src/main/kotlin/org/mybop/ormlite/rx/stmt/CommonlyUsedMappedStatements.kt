package org.mybop.ormlite.rx.stmt

import com.j256.ormlite.db.DatabaseType
import com.j256.ormlite.stmt.mapped.*
import com.j256.ormlite.table.TableInfo
import io.reactivex.Single

/**
 * Helper class preparing and caching commonly used mapped statements.
 *
 * @param T The class that the code will be operating on.
 * @param ID The class of the ID column associated with the class.
 * @param databaseType Type of the database on which we operate.
 * @param tableInfo Information about the table we will query/modify with the mapped statements.
 */
class CommonlyUsedMappedStatements<T, ID>(
        private val databaseType: DatabaseType,
        private val tableInfo: TableInfo<T, ID>
) {

    /**
     * [Single] emitting the mapped create to use when inserting into the table.
     */
    val mappedCreate: Single<MappedCreate<T, ID>> = Single.fromCallable { MappedCreate.build(databaseType, tableInfo) }
            .cache()

    /**
     * [Single] emitting the mapped update to use when updating the table.
     */
    val mappedUpdate: Single<MappedUpdate<T, ID>> = Single.fromCallable { MappedUpdate.build(databaseType, tableInfo) }
            .cache()

    /**
     * [Single] emitting the mapped update to use when updating the ID field of a row in the table.
     */
    val mappedUpdateId: Single<MappedUpdateId<T, ID>> = Single.fromCallable { MappedUpdateId.build(databaseType, tableInfo) }
            .cache()

    /**
     * [Single] emitting the mapped refresh to use when refreshing a data item from the table.
     */
    val mappedRefresh: Single<MappedRefresh<T, ID>> = Single.fromCallable { MappedRefresh.build(databaseType, tableInfo) }
            .cache()

    /**
     * [Single] emitting the mapped delete to use when deleting from the table.
     */
    val mappedDelete: Single<MappedDelete<T, ID>> = Single.fromCallable { MappedDelete.build(databaseType, tableInfo) }
            .cache()
}
