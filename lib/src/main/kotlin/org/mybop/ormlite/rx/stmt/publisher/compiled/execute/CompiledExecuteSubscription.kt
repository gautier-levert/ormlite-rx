package org.mybop.ormlite.rx.stmt.publisher.compiled.execute

import com.j256.ormlite.support.CompiledStatement
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import org.mybop.ormlite.rx.stmt.publisher.ConnectionBackedSubscription
import org.reactivestreams.Subscriber

/**
 * Subscriber that executes the provided statement and publishes the number of rows modified/deleted when it is
 * requested.
 *
 * @param subscriber [Subscriber] to emit the number of rows to.
 * @param connectionSource An open [ConnectionSource] to the database.
 * @param connection [DatabaseConnection] on which the mapped statement will be executed.
 * @param compiledStatement Statement to execute.
 */
class CompiledExecuteSubscription(
        private val subscriber: Subscriber<in Int>,
        connectionSource: ConnectionSource,
        private val connection: DatabaseConnection,
        private val compiledStatement: CompiledStatement,
        private val update: Boolean
) : ConnectionBackedSubscription<Int>(subscriber, connectionSource, connection) {

    override fun doRequest(n: Long) {
        val result = if (update) compiledStatement.runUpdate() else compiledStatement.runExecute()

        subscriber.onNext(result)
        subscriber.onComplete()
        closeConnectionQuietly()
    }

    override fun close() {
        compiledStatement.closeQuietly()
    }
}
