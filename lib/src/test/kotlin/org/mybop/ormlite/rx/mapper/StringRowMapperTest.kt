package org.mybop.ormlite.rx.mapper

import com.j256.ormlite.support.DatabaseResults
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import org.junit.Assert.assertEquals
import org.junit.Test

class StringRowMapperTest {

    @Test
    fun mapRow() {
        val results = mock<DatabaseResults> {
            on { it.getString(0) } doReturn "test"
        }

        val mapper = StringRowMapper
        assertEquals("test", mapper.mapRow(results))
    }
}
