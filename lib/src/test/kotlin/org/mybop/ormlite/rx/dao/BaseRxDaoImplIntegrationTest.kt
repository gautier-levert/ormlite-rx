package org.mybop.ormlite.rx.dao

import com.j256.ormlite.dao.Dao
import com.j256.ormlite.dao.DaoManager
import com.j256.ormlite.jdbc.JdbcConnectionSource
import com.j256.ormlite.table.TableUtils
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mybop.ormlite.rx.mapper.LongRowMapper
import org.mybop.ormlite.rx.model.TestModel

class BaseRxDaoImplIntegrationTest {

    private var connectionSource: JdbcConnectionSource? = null

    @Before
    fun setUp() {
        connectionSource = JdbcConnectionSource("jdbc:h2:mem:test;DB_CLOSE_DELAY=0")

        TableUtils.createTable(connectionSource, TestModel::class.java)
    }

    @After
    fun tearDown() {
        connectionSource?.closeQuietly()
    }

    @Test
    fun queryForId() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModels = (1L..10).map {
            val testModel = TestModel()
            testModel.id = it
            testModel.value = it * 10
            testModel
        }

        testModelDao.create(testModels)

        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource!!, TestModel::class.java)

        testModelRxDao.queryForId(5L)
                .subscribe {
                    assertThat(it.id).isEqualTo(5L)
                    assertThat(it.value).isEqualTo(50L)
                }
    }

    @Test
    fun queryForFirst() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModels = (1L..10).map {
            val testModel = TestModel()
            testModel.id = it
            testModel.value = it * 10
            testModel
        }

        testModelDao.create(testModels)

        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource!!, TestModel::class.java)

        testModelRxDao.queryForFirst(
                testModelDao.queryBuilder()
                        .orderBy("value", true)
                        .where().ge("value", 40)
                        .prepare()
        )
                .subscribe {
                    assertThat(it.id).isEqualTo(4L)
                    assertThat(it.value).isEqualTo(40L)
                }
    }

    @Test
    fun queryForAll() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModels = (1L..10).map {
            val testModel = TestModel()
            testModel.id = it
            testModel.value = it * 10
            testModel
        }

        testModelDao.create(testModels)

        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource!!, TestModel::class.java)

        val databaseModels = testModelRxDao.queryForAll().blockingIterable().toList()

        assertThat(databaseModels).containsOnlyElementsOf(testModels)
    }

    @Test
    fun query() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModels = (1L..10).map {
            val testModel = TestModel()
            testModel.id = it
            testModel.value = it * 10
            testModel
        }

        testModelDao.create(testModels)

        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource!!, TestModel::class.java)

        val databaseModels = testModelRxDao.query(
                testModelDao.queryBuilder()
                        .orderBy("value", true)
                        .where().ge("value", 40)
                        .prepare()
        )
                .blockingIterable()
                .toList()

        assertThat(databaseModels).containsOnlyElementsOf(testModels.filter { it.value >= 40 })
    }

    @Test
    fun create() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource!!, TestModel::class.java)

        val testModel = TestModel()
        testModel.id = 1
        testModel.value = 10

        testModelRxDao.create(testModel).blockingGet()

        assertThat(testModelDao.queryForId(testModel.id)).isNotNull()
    }

    @Test
    fun createIfNotExists_create() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource!!, TestModel::class.java)

        val testModel = TestModel()
        testModel.id = 1
        testModel.value = 10

        testModelRxDao.createIfNotExists(testModel).blockingGet()

        assertThat(testModelDao.queryForId(testModel.id)).isNotNull()
    }

    @Test
    fun createIfNotExists_exists() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource!!, TestModel::class.java)

        val existingTestModel = TestModel()
        existingTestModel.id = 1
        existingTestModel.value = 10

        testModelDao.create(existingTestModel)

        val testModel = TestModel()
        testModel.id = 1
        testModel.value = 100

        testModelRxDao.createIfNotExists(testModel)
                .blockingGet()

        assertThat(testModel.value).isEqualTo(existingTestModel.value)
    }

    @Test
    fun createOrUpdate_create() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource!!, TestModel::class.java)

        val testModel = TestModel()
        testModel.id = 1
        testModel.value = 10

        testModelRxDao.createOrUpdate(testModel).blockingGet()

        assertThat(testModelDao.queryForId(testModel.id)).isNotNull()
    }

    @Test
    fun createOrUpdate_update() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource!!, TestModel::class.java)

        val existingTestModel = TestModel()
        existingTestModel.id = 1
        existingTestModel.value = 10

        testModelDao.create(existingTestModel)

        val testModel = TestModel()
        testModel.id = 1
        testModel.value = 100

        testModelRxDao.createOrUpdate(testModel).blockingGet()

        val newTestModel = testModelDao.queryForId(existingTestModel.id)
        assertThat(newTestModel.value).isEqualTo(testModel.value)
    }

    @Test
    fun update() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource!!, TestModel::class.java)

        val testModel = TestModel()
        testModel.id = 1
        testModel.value = 10

        testModelDao.create(testModel)

        testModel.value = 100

        val updatedTestModel = testModelRxDao.update(testModel)
                .blockingGet()
        assertThat(testModel.value).isEqualTo(updatedTestModel.value)

        val dbTestModel = testModelDao.queryForId(testModel.id)
        assertThat(testModel.value).isEqualTo(dbTestModel.value)
    }

    @Test
    fun updateId() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource!!, TestModel::class.java)

        val testModel = TestModel()
        testModel.id = 1
        testModel.value = 10

        testModelDao.create(testModel)

        val updatedTestModel = testModelRxDao.updateId(testModel, 2)
                .blockingGet()
        assertThat(updatedTestModel.id).isEqualTo(2L)

        assertThat(testModelDao.queryForId(1)).isNull()
        assertThat(testModelDao.queryForId(2)).isNotNull()
    }

    @Test
    fun updatePrepared() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource!!, TestModel::class.java)

        val testModels = (1..10L).map {
            val testModel = TestModel()
            testModel.id = it
            testModel.value = it * 10
            testModel
        }
        testModelDao.create(testModels)

        testModelRxDao.updateBuilder()
                .updateColumnValue("value", 200)
                .where()
                .lt("id", 5)
                .prepare()
                .flatMapCompletable { testModelRxDao.update(it) }
                .blockingAwait()

        val dbTestModels = testModelDao.queryForAll()
        assertThat(dbTestModels.filter { it.id!! < 5L }.all { it.value == 200L }).isTrue()
    }

    @Test
    fun refresh() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource!!, TestModel::class.java)

        val testModel = TestModel()
        testModel.id = 1
        testModel.value = 10

        testModelDao.create(testModel)

        testModel.value = 100

        val updatedTestModel = testModelRxDao.refresh(testModel)
                .blockingGet()
        assertThat(testModel.value).isEqualTo(10L)
        assertThat(updatedTestModel.value).isEqualTo(10L)
    }

    @Test
    fun delete() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource!!, TestModel::class.java)

        val testModel = TestModel()
        testModel.id = 1
        testModel.value = 10

        testModelDao.create(testModel)

        testModelRxDao.delete(testModel).blockingAwait()

        assertThat(testModelDao.queryForId(testModel.id)).isNull()
    }

    @Test
    fun deleteById() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource!!, TestModel::class.java)

        val testModel = TestModel()
        testModel.id = 1
        testModel.value = 10

        testModelDao.create(testModel)

        testModelRxDao.deleteById(testModel.id!!).blockingAwait()

        assertThat(testModelDao.queryForId(testModel.id)).isNull()
    }

    @Test
    fun deleteObjects() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource!!, TestModel::class.java)

        val testModels = (1..10).map {
            val testModel = TestModel()
            testModel.id = it.toLong()
            testModel.value = it.toLong()
            testModel
        }

        testModelDao.create(testModels)

        val deletedTestModels = testModels.filter { it.id!! <= 5L }
        testModelRxDao.delete(deletedTestModels).blockingAwait()

        val dbTestModels = testModelDao.queryForAll()
        assertThat(dbTestModels.size).isEqualTo(5)
        assertThat(dbTestModels.containsAll(deletedTestModels)).isFalse()
    }

    @Test
    fun deleteIds() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource!!, TestModel::class.java)

        val testModels = (1..10).map {
            val testModel = TestModel()
            testModel.id = it.toLong()
            testModel.value = it.toLong()
            testModel
        }

        testModelDao.create(testModels)

        val deletedTestModels = testModels.filter { it.id!! <= 5L }
        testModelRxDao.deleteIds(deletedTestModels.map { it.id!! }).blockingAwait()

        val dbTestModels = testModelDao.queryForAll()
        assertThat(dbTestModels.size).isEqualTo(5)
        assertThat(dbTestModels.containsAll(deletedTestModels)).isFalse()
    }

    @Test
    fun deletePrepared() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource!!, TestModel::class.java)

        val testModels = (1..10).map {
            val testModel = TestModel()
            testModel.id = it.toLong()
            testModel.value = it.toLong()
            testModel
        }

        testModelDao.create(testModels)

        testModelRxDao.deleteBuilder()
                .where()
                .lt("id", 5)
                .prepare()
                .flatMapCompletable { testModelRxDao.delete(it) }
                .blockingAwait()

        val dbTestModels = testModelDao.queryForAll()
        assertThat(dbTestModels.size).isEqualTo(6)
        assertThat(dbTestModels.filter { it.id!! < 5L }.count()).isEqualTo(0)
    }

    @Test
    fun countOf() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModels = (1L..10).map {
            val testModel = TestModel()
            testModel.id = it
            testModel.value = it * 10
            testModel
        }

        testModelDao.create(testModels)

        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource!!, TestModel::class.java)

        assertThat(testModelRxDao.countOf().blockingGet()).isEqualTo(10L)
    }

    @Test
    fun countOf_query() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModels = (1L..10).map {
            val testModel = TestModel()
            testModel.id = it
            testModel.value = it * 10
            testModel
        }

        testModelDao.create(testModels)

        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource!!, TestModel::class.java)

        assertThat(
                testModelRxDao
                        .countOf(
                                testModelDao.queryBuilder()
                                        .where()
                                        .ge("value", 50)
                                        .prepare()
                        )
                        .blockingGet()
        ).isEqualTo(5L)
    }

    @Test
    fun queryRaw() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModels = (1L..10).map {
            val testModel = TestModel()
            testModel.id = it
            testModel.value = it * 10
            testModel
        }

        testModelDao.create(testModels)

        val testModelRxDao = RxDaoManager.createDao<TestModel, Long>(connectionSource!!, TestModel::class.java) as BaseRxDaoImpl<TestModel, Long>

        val databaseIds = testModelRxDao.queryRaw("SELECT id FROM test WHERE id > ?", LongRowMapper, "5")
                .blockingIterable().toList()

        assertThat(databaseIds).containsOnlyElementsOf(6L..10)
    }

    @Test
    fun queryRawFirst() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModels = (1L..10).map {
            val testModel = TestModel()
            testModel.id = it
            testModel.value = it * 10
            testModel
        }

        testModelDao.create(testModels)

        val testModelRxDao = RxDaoManager.createDao<TestModel, Long>(connectionSource!!, TestModel::class.java) as BaseRxDaoImpl<TestModel, Long>

        val result = testModelRxDao.queryRawFirst("SELECT id FROM test WHERE id > ?", LongRowMapper, "5")
                .blockingGet()

        assertThat(result).isEqualTo(6)
    }

    @Test
    fun executeRaw() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModel = TestModel()
        testModel.id = 1
        testModel.value = 10

        testModelDao.create(testModel)

        val testModelRxDao = RxDaoManager.createDao<TestModel, Long>(connectionSource!!, TestModel::class.java) as BaseRxDaoImpl<TestModel, Long>

        testModelRxDao.executeRaw("UPDATE test SET value = ? WHERE id = ?", "100", "1")
                .blockingAwait()

        val dbTestModel = testModelDao.queryForId(1)
        assertThat(dbTestModel.value).isEqualTo(100)
    }

    @Test
    fun updateRaw() {
        val testModelDao: Dao<TestModel, Long> = DaoManager.createDao(connectionSource, TestModel::class.java)
        testModelDao.objectCache = null

        val testModel = TestModel()
        testModel.id = 1
        testModel.value = 10

        testModelDao.create(testModel)

        val testModelRxDao = RxDaoManager.createDao<TestModel, Long>(connectionSource!!, TestModel::class.java) as BaseRxDaoImpl<TestModel, Long>

        testModelRxDao.updateRaw("UPDATE test SET value = ? WHERE id = ?", "100", "1")
                .blockingAwait()

        val dbTestModel = testModelDao.queryForId(1)
        assertThat(dbTestModel.value).isEqualTo(100)
    }
}
