package org.mybop.ormlite.rx.example.datapersister

import com.j256.ormlite.field.FieldType
import com.j256.ormlite.field.SqlType
import com.j256.ormlite.field.types.BaseDataType
import com.j256.ormlite.support.DatabaseResults
import org.joda.time.Instant

class InstantPersister : BaseDataType(SqlType.LONG, arrayOf(Instant::class.java)) {

    override fun resultToSqlArg(fieldType: FieldType, results: DatabaseResults, columnPos: Int): Long = results.getLong(columnPos)

    override fun javaToSqlArg(fieldType: FieldType, javaObject: Any): Long = (javaObject as Instant).millis

    override fun sqlArgToJava(fieldType: FieldType, sqlArg: Any, columnPos: Int): Instant = Instant(sqlArg as Long)

    override fun isStreamType(): Boolean = false

    override fun isEscapedValue(): Boolean = false

    override fun parseDefaultString(fieldType: FieldType, defaultStr: String): Instant = Instant.parse(defaultStr)
}
