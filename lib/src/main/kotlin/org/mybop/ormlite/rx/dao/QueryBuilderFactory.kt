package org.mybop.ormlite.rx.dao

import com.j256.ormlite.stmt.PreparedDelete
import com.j256.ormlite.stmt.PreparedQuery
import com.j256.ormlite.stmt.PreparedUpdate
import org.mybop.ormlite.rx.stmt.RxDeleteBuilder
import org.mybop.ormlite.rx.stmt.RxQueryBuilder
import org.mybop.ormlite.rx.stmt.RxUpdateBuilder

/**
 * The definition of the query builder factory that handle creation of [RxQueryBuilder], [RxUpdateBuilder] and [RxDeleteBuilder].
 * Query builders assist preparation of [PreparedQuery], [PreparedUpdate] and [PreparedDelete] that will be passed to [RxDao] methods.
 *
 * @param <T>
 *            The class that the code will be operating on.
 * @param <ID>
 *            The class of the ID column associated with the class. The T class does not require an ID field. The class
 *            needs an ID parameter however so you can use Void or Object to satisfy the compiler.
 */
interface QueryBuilderFactory<T, ID> {
    /**
     * Create and return a new [RxQueryBuilder] object which allows you to build a custom query. You call methods on
     * the [RxQueryBuilder] to construct your custom query and then call [RxQueryBuilder.prepare] once you
     * are ready to build your query. This returns a [PreparedQuery] object which gets passed to
     * [RxDao.query].
     */
    fun queryBuilder(): RxQueryBuilder<T, ID>

    /**
     * Like [queryBuilder] but allows you to build an UPDATE statement. You can then call call
     * [RxUpdateBuilder.prepare] and pass the returned [PreparedUpdate] to [RxDao.update].
     */
    fun updateBuilder(): RxUpdateBuilder<T, ID>

    /**
     * Like [queryBuilder] but allows you to build an DELETE statement. You can then call call
     * [RxDeleteBuilder.prepare] and pass the returned [PreparedDelete] to [RxDao.delete].
     */
    fun deleteBuilder(): RxDeleteBuilder<T, ID>
}
