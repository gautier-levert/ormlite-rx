package org.mybop.ormlite.rx.dao

import com.j256.ormlite.stmt.PreparedDelete
import com.j256.ormlite.stmt.PreparedQuery
import com.j256.ormlite.stmt.PreparedUpdate
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import org.mybop.ormlite.rx.stmt.RxQueryBuilder
import org.mybop.ormlite.rx.stmt.RxUpdateBuilder

/**
 * The definition of the Database Access Objects that handle the reading and writing a class from the database.
 * @param <T>
 *            The class that the code will be operating on.
 * @param <ID>
 *            The class of the ID column associated with the class. The T class does not require an ID field. The class
 *            needs an ID parameter however so you can use Void or Object to satisfy the compiler.
 *
 * @see com.j256.ormlite.dao.Dao
 */
interface RxDao<T, ID> : QueryBuilderFactory<T, ID> {

    /**
     * Retrieves an object associated with a specific ID.

     * @param id Identifier that matches a specific row in the database to find and return.
     *
     * @return A [Maybe] emitting the object that has the ID field which equals id or null if no matches.
     */
    fun queryForId(id: ID): Maybe<T>

    /**
     * Query for and return the first item in the object table which matches the [PreparedQuery]. See
     * [queryBuilder] for more information. This can be used to return the object that matches a single unique
     * column. You should use [queryForId] if you want to query for the id column.
     *
     * @param preparedQuery Query used to match the objects in the database.
     * @return A [Maybe] emitting the first object that matches the query.
     */
    fun queryForFirst(preparedQuery: PreparedQuery<T>): Maybe<T>

    /**
     * Query for all of the items in the object table.

     * @return A [Flowable] emitting all the objects in the table.
     */
    fun queryForAll(): Flowable<T>

    /**
     * Query for the items in the object table which match the [PreparedQuery]. See [queryBuilder] for more
     * information.
     *
     * @param preparedQuery Query used to match the objects in the database.
     * @return A [Flowable] emitting all the objects in the table that match the query.
     */
    fun query(preparedQuery: PreparedQuery<T>): Flowable<T>

    /**
     * Create a new row in the database from an object.
     *
     * @param data  The data item that we are creating in the database.
     * @return A [Single] emitting the data item we have created in the database.
     */
    fun create(data: T): Single<T>

    /**
     * Just like [create] but with a collection of objects. This will wrap the creates using the same
     * mechanism as [Dao.callBatchTasks][com.j256.ormlite.dao.Dao.callBatchTasks].
     *
     * @param dataCollection The collection of data items that we are creating in the database.
     * @return A [Completable] completing in case of success or emitting an error otherwise.
     */
    fun create(dataCollection: Collection<T>): Completable

    /**
     * This is a convenience method to creating a data item but only if the ID does not already exist in the table. This
     * extracts the id from the data parameter, does a [queryForId] on it, returning the data if it
     * exists. If it does not exist [create] will be called with the parameter.
     *
     * @return A [Single] emitting either the data parameter if it was inserted (now with the ID field set via the
     *         create method) or the data element that existed already in the database.
     */
    fun createIfNotExists(data: T): Single<T>

    /**
     * This is a convenience method for creating an item in the database if it does not exist. The id is extracted from
     * the data parameter and a query-by-id is made on the database. If a row in the database with the same id exists
     * then all of the columns in the database will be updated from the fields in the data parameter. If the id is null
     * (or 0 or some other default value) or doesn't exist in the database then the object will be created in the
     * database. This also means that your data item <i>must</i> have an id field defined.
     *
     * @return A [Single] emitting emitting the data parameter if it was inserted or updated. The ID field may have
     * been set if it is auto-generated.
     */
    fun createOrUpdate(data: T): Single<T>

    /**
     * Save the fields from an object to the database. If you have made changes to an object, this is how you persist
     * those changes to the database. You cannot use this method to update the id field -- see [updateId].
     *
     * @param data The data item that we are updating in the database.
     * @return A [Single] emitting the updated data item.
     */
    fun update(data: T): Single<T>

    /**
     * Update an object in the database to change its id to the newId parameter. The data <i>must</i> have its current
     * id set. If the id field has already changed then it cannot be updated. After the id has been updated in the
     * database, the id field of the data object will also be changed.
     *
     * <p>
     * <b>NOTE:</b> Depending on the database type and the id type, you may be unable to change the id of the field.
     * </p>
     *
     * @param data The data item that we are updating in the database with the current id.
     * @param newId The <i>new</i> id that you want to update the data with.
     * @return A [Completable] completing in case of success or emitting an error otherwise.
     */
    fun updateId(data: T, newId: ID): Single<T>

    /**
     * Update all rows in the table according to the prepared statement parameter. To use this, the
     * [RxUpdateBuilder] must have set-columns applied to it using the
     * [RxUpdateBuilder.updateColumnValue] or
     * [RxUpdateBuilder.updateColumnExpression] methods.
     *
     * @param preparedUpdate A prepared statement to match database rows to be deleted and define the columns to update.
     * @return A [Completable] completing in case of success or emitting an error otherwise.
     */
    fun update(preparedUpdate: PreparedUpdate<T>): Completable

    /**
     * Does a query for the object's id and copies in each of the field values from the database to refresh the data
     * parameter. Any local object changes to persisted fields will be overwritten. If the database has been updated
     * this brings your local object up to date.
     *
     * @param data The data item that we are refreshing with fields from the database.
     * @return A [Single] emitting the data item with fields refreshed from the database.
     */
    fun refresh(data: T): Single<T>

    /**
     * Delete an object from the database.
     *
     * @param data The data item that we are deleting from the database.
     * @return A [Completable] completing in case of success or emitting an error otherwise.
     */
    fun delete(data: T): Completable

    /**
     * Delete a collection of objects from the database using an IN SQL clause.
     *
     * @param dataCollection A collection of data items to be deleted.
     * @return A [Completable] completing in case of success or emitting an error otherwise.
     */
    fun delete(dataCollection: Collection<T>): Completable

    /**
     * Delete an object from the database that has an id.
     *
     * @param id The id of the item that we are deleting from the database.
     * @return A [Completable] completing in case of success or emitting an error otherwise.
     */
    fun deleteById(id: ID): Completable

    /**
     * Delete the objects that match the collection of ids from the database using an IN SQL clause.
     *
     * @param idsCollection A collection of data ids to be deleted.
     * @return A [Completable] completing in case of success or emitting an error otherwise.
     */
    fun deleteIds(idsCollection: Collection<ID>): Completable

    /**
     * Delete the objects that match the prepared statement parameter.
     *
     * @param preparedDelete A prepared statement to match database rows to be deleted.
     * @return A [Completable] completing in case of success or emitting an error otherwise.
     */
    fun delete(preparedDelete: PreparedDelete<T>): Completable

    /**
     * Returns the number of rows in the table associated with the data class. Depending on the size of the table and
     * the database type, this may be expensive and take a while.
     *
     * @return A [Single] emitting the number of rows in the table.
     */
    fun countOf(): Single<Long>

    /**
     * Returns the number of rows in the table associated with the prepared query passed in. Depending on the size of
     * the table and the database type, this may be expensive and take a while.
     *
     * <p>
     * <b>NOTE:</b> If the query was prepared with the [RxQueryBuilder] then you should have called the
     * [RxQueryBuilder.setCountOf] with true before you prepared the query. You may instead want to use
     * [RxQueryBuilder.countOf] which makes it all easier.
     * </p>
     *
     * @return A [Single] emitting the number of rows that match the query.
     */
    fun countOf(preparedQuery: PreparedQuery<T>): Single<Long>
}
