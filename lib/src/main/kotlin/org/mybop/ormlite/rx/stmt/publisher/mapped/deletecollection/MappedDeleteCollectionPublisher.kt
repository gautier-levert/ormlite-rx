package org.mybop.ormlite.rx.stmt.publisher.mapped.deletecollection

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.db.DatabaseType
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import com.j256.ormlite.table.TableInfo
import io.reactivex.internal.subscriptions.EmptySubscription
import org.mybop.ormlite.rx.stmt.publisher.mapped.MappedStatementPublisher
import org.reactivestreams.Publisher
import org.reactivestreams.Subscriber
import java.util.concurrent.atomic.AtomicInteger

/**
 * Publisher that will delete all the data and emits the number of rows deleted from the database.
 *
 * @param T The class that the code will be operating on.
 * @param ID The class of the ID column associated with the class.
 * @param databaseType Type of the database on which the mapped statement will be executed.
 * @param tableInfo [TableInfo] of the table from which we will delete the items.
 * @param connectionSource An open [ConnectionSource] to the database.
 * @param dataCollection Collection of data to delete with their ID field set. Either this or idCollection parameter must be provided.
 * @param idCollection Collection of ID to delete. Either this or dataCollection parameter must be provided.
 * @param objectCache Optional [ObjectCache].
 *
 * @see MappedStatementPublisher
 * @see MappedDeleteCollectionSubscription
 */
class MappedDeleteCollectionPublisher<T, ID>(
        private val databaseType: DatabaseType,
        private val tableInfo: TableInfo<T, ID>,
        private val connectionSource: ConnectionSource,
        private val dataCollection: Collection<T>?,
        private val idCollection: Collection<ID>?,
        private val objectCache: ObjectCache?
) : Publisher<Int> {

    private val atomicSubscriptionCount = AtomicInteger()

    init {
        if (dataCollection == null && idCollection == null) {
            throw IllegalArgumentException("Either dataCollection of idCollection must be specified.")
        }
    }

    override fun subscribe(subscriber: Subscriber<in Int>) {
        val subscriptionCount = atomicSubscriptionCount.incrementAndGet()
        if (subscriptionCount > 1) {
            throw IllegalStateException("Multiple subscription on create/update/delete is not allowed.")
        }

        var connection: DatabaseConnection? = null
        try {
            connection = connectionSource.getReadWriteConnection(tableInfo.tableName)
            val subscription = createSubscription(subscriber, connection)
            subscriber.onSubscribe(subscription)
        } catch (t: Throwable) {
            EmptySubscription.error(t, subscriber)
            connection?.closeQuietly()
        }
    }

    private fun createSubscription(subscriber: Subscriber<in Int>, connection: DatabaseConnection)
            = MappedDeleteCollectionSubscription(subscriber, databaseType, tableInfo, connectionSource,
            connection, dataCollection, idCollection, objectCache)
}
