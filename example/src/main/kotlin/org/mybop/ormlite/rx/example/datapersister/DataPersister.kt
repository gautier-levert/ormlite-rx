package org.mybop.ormlite.rx.example.datapersister

import com.j256.ormlite.field.DataPersisterManager
import com.j256.ormlite.jdbc.JdbcConnectionSource
import com.j256.ormlite.table.TableUtils
import org.joda.time.Instant
import org.mybop.ormlite.rx.dao.RxDaoManager

const val DATABASE_URL = "jdbc:h2:mem:account"

fun main(vararg args: String) {
    DataPersisterManager.registerDataPersisters(InstantPersister())

    JdbcConnectionSource(DATABASE_URL).use { connectionSource ->
        val accountDao = RxDaoManager.createDao<Account, Instant>(connectionSource, Account::class.java)

        // if you need to create the table
        TableUtils.createTable(connectionSource, Account::class.java)

        val toto = Account(name = "Toto", password = "p@ssw0rd")

        accountDao.create(toto)
                .subscribe()

        accountDao.queryForAll()
                .subscribe { account ->
                    println(account)
                }

        toto.name = "paul"

        accountDao.createIfNotExists(toto)
                .subscribe({}, { exception ->
                    exception.printStackTrace()
                })

        accountDao.queryForAll()
                .subscribe { account ->
                    println(account)
                }

        toto.password = "P1wd"

        accountDao.createOrUpdate(toto)
                .subscribe({}, { exception ->
                    exception.printStackTrace()
                })

        accountDao.queryForAll()
                .subscribe { account ->
                    println(account)
                }
    }
}
