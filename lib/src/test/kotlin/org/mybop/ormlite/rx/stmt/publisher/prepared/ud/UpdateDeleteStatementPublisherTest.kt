package org.mybop.ormlite.rx.stmt.publisher.prepared.ud

import com.j256.ormlite.stmt.PreparedStmt
import com.j256.ormlite.stmt.StatementBuilder
import com.j256.ormlite.support.CompiledStatement
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import com.nhaarman.mockito_kotlin.*
import org.junit.Test
import org.mybop.ormlite.rx.stmt.publisher.compiled.execute.CompiledExecuteSubscription
import org.reactivestreams.Subscriber
import java.sql.SQLException

class UpdateDeleteStatementPublisherTest {

    @Test
    fun subscribe() {
        val tableName = "test"
        val statementType = StatementBuilder.StatementType.UPDATE
        val subscriber = mock<Subscriber<Int>>()
        val connection = mock<DatabaseConnection>()
        val connectionSource = mock<ConnectionSource> {
            on { getReadOnlyConnection("test") } doReturn connection
        }
        val compiledStatement = mock<CompiledStatement>()
        val preparedStatement = mock<PreparedStmt<Any>> {
            on { it.compile(connection, statementType) } doReturn compiledStatement
            on { it.type } doReturn statementType
        }

        val publisher = UpdateDeleteStatementPublisher(
                connectionSource,
                tableName,
                preparedStatement
        )

        publisher.subscribe(subscriber)
        verify(connectionSource).getReadOnlyConnection(tableName)
        verify(preparedStatement).compile(connection, statementType)
        verify(subscriber).onSubscribe(any<CompiledExecuteSubscription>())
    }

    @Test
    fun subscribe_connectionThrowsException() {
        val tableName = "test"
        val statementType = StatementBuilder.StatementType.UPDATE
        val subscriber = mock<Subscriber<Int>>()
        val connectionSource = mock<ConnectionSource> {
            on { getReadOnlyConnection(tableName) } doThrow SQLException()
        }
        val preparedStatement = mock<PreparedStmt<Any>> {
            on { it.type } doReturn statementType
        }

        val publisher = UpdateDeleteStatementPublisher(
                connectionSource,
                tableName,
                preparedStatement
        )

        publisher.subscribe(subscriber)
        verify(subscriber).onError(any())
    }

    @Test
    fun subscribe_compileStatementThrowsException() {
        val tableName = "test"
        val statementType = StatementBuilder.StatementType.UPDATE
        val subscriber = mock<Subscriber<Int>>()
        val connection = mock<DatabaseConnection>()
        val connectionSource = mock<ConnectionSource> {
            on { getReadOnlyConnection(tableName) } doReturn connection
        }
        val preparedStatement = mock<PreparedStmt<Any>> {
            on { it.compile(connection, statementType) } doThrow SQLException()
            on { it.type } doReturn statementType
        }

        val publisher = UpdateDeleteStatementPublisher(
                connectionSource,
                tableName,
                preparedStatement
        )

        publisher.subscribe(subscriber)
        verify(subscriber).onError(any())
        verify(connectionSource).releaseConnection(connection)
    }
}
