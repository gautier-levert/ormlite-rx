package org.mybop.ormlite.rx.stmt.publisher.mapped

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.stmt.mapped.BaseMappedStatement
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import org.mybop.ormlite.rx.stmt.publisher.ConnectionBackedSubscription
import org.reactivestreams.Subscriber

/**
 * Abstract subscription that will execute the statement, emit the result and close the underlying
 * database connection when requested by the publisher.
 *
 * @param T The class that the code will be operating on.
 * @param ID The class of the ID column associated with the class.
 * @param R Type of the result of the mapped statement.
 * @param Q Type of the mapped statement that will be executed. Must be a subclass of [BaseMappedStatement].
 * @param subscriber Subscriber to emit the result of the statement to.
 * @param connectionSource [ConnectionSource] that have provided the connection.
 * @param connection [DatabaseConnection] on which the mapped statement will be executed.
 * @param mappedStatement Mapped statement that will be executed when a publisher subscribes and requests.
 * @param objectCache Optional [ObjectCache].
 */
abstract class MappedStatementSubscription<T, ID, R,
        in Q : BaseMappedStatement<T, ID>>(
        private val subscriber: Subscriber<in R>,
        connectionSource: ConnectionSource,
        private val connection: DatabaseConnection,
        private val mappedStatement: Q,
        private val objectCache: ObjectCache?
) : ConnectionBackedSubscription<R>(subscriber, connectionSource, connection) {

    /**
     * Execute the mappedStatement and return its result.
     * @param mappedStatement Statement to execute
     * @param connection Database connection on which the statement is executed
     * @param objectCache Object cache to use for the statement
     * @return Result of the statement
     */
    abstract fun execute(mappedStatement: Q, connection: DatabaseConnection, objectCache: ObjectCache?): R

    override fun doRequest(n: Long) {
        val result = execute(mappedStatement, connection, objectCache)
        subscriber.onNext(result)
        subscriber.onComplete()
        closeConnectionQuietly()
    }

    override fun close() {}
}
