package org.mybop.ormlite.rx.stmt

import com.j256.ormlite.dao.Dao
import com.j256.ormlite.stmt.PreparedUpdate
import com.j256.ormlite.stmt.UpdateBuilder
import io.reactivex.Completable
import io.reactivex.Single
import org.mybop.ormlite.rx.dao.RxDao
import org.mybop.ormlite.rx.stmt.where.RxUpdateWhere
import java.sql.SQLException

/**
 * Assists in building sql UPDATE statements for a particular table in a particular database.
 *
 * @param <T>
 *            The class that the code will be operating on.
 * @param <ID>
 *            The class of the ID column associated with the class. The T class does not require an ID field. The class
 *            needs an ID parameter however so you can use Void or Object to satisfy the compiler.
 * @see UpdateBuilder
 */
class RxUpdateBuilder<T, ID>(
        private val updateBuilder: UpdateBuilder<T, ID>,
        private val rxDao: RxDao<T, ID>
) : RxStatementBuilder<T, ID, RxUpdateWhere<T, ID>>(updateBuilder) {

    override fun where(): RxUpdateWhere<T, ID> = RxUpdateWhere(updateBuilder.where(), this)

    /**
     * Build and return a prepared update that can be used by [Dao.update] method. If you change
     * the where or make other calls you will need to re-call this method to re-prepare the statement for execution.
     */
    override fun prepare(): Single<PreparedUpdate<T>> = Single.fromCallable { updateBuilder.prepare() }

    /**
     * Add a column to be set to a value for UPDATE statements. This will generate something like columnName = 'value'
     * with the value escaped if necessary.
     */
    @Throws(SQLException::class)
    fun updateColumnValue(columnName: String, value: Any): RxUpdateBuilder<T, ID> {
        updateBuilder.updateColumnValue(columnName, value)
        return this
    }

    /**
     * Add a column to be set to a value for UPDATE statements. This will generate something like 'columnName =
     * expression' where the expression is built by the caller.
     *
     * The expression should have any strings escaped using the [escapeValue] or
     * [escapeValue] methods and should have any column names escaped using the
     * [escapeColumnName] or [escapeColumnName] methods.
     */
    @Throws(SQLException::class)
    fun updateColumnExpression(columnName: String, expression: String): RxUpdateBuilder<T, ID> {
        updateBuilder.updateColumnExpression(columnName, expression)
        return this
    }

    /**
     * When you are building the expression for [updateColumnExpression], you may need to escape
     * column names since they may be reserved words to the database. This will help you by adding escape characters
     * around the word.
     */
    fun escapeColumnName(sb: StringBuilder, columnName: String) {
        updateBuilder.escapeColumnName(sb, columnName)
    }

    /**
     * Same as [escapeColumnName] but it will return the escaped string. The StringBuilder
     * method is more efficient since this method creates a StringBuilder internally.
     */
    fun escapeColumnName(columnName: String): String = updateBuilder.escapeColumnName(columnName)

    /**
     * When you are building the expression for [updateColumnExpression], you may need to escape
     * values since they may be reserved words to the database. Numbers should not be escaped. This will help you by
     * adding escape characters around the word.
     */
    fun escapeValue(sb: StringBuilder, value: String) {
        updateBuilder.escapeValue(sb, value)
    }

    /**
     * Same as [escapeValue] but it will return the escaped string. Numbers should not be
     * escaped. The StringBuilder method is more efficient since this method creates a StringBuilder internally.
     */
    fun escapeValue(value: String): String = updateBuilder.escapeValue(value)

    /**
     * A short cut to [Dao.update].
     */
    fun update(): Completable {
        return prepare().flatMapCompletable { rxDao.update(it) }
    }
}
