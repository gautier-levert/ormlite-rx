package org.mybop.ormlite.rx.misc

import com.j256.ormlite.logger.LoggerFactory
import com.j256.ormlite.support.ConnectionSource
import io.reactivex.Completable
import java.sql.SQLException
import java.sql.Savepoint
import java.util.concurrent.atomic.AtomicInteger

/**
 * Wrapper around completable to subscribe in transaction.
 *
 * Must be used by calling [RxTransactionManager.callInTransaction][org.mybop.ormlite.rx.misc.RxTransactionManager.callInTransaction]
 */
internal class TransactionCompletable(
        private val connectionSource: ConnectionSource,
        tableName: String?,
        completable: Completable
) : BatchCompletable(connectionSource, tableName, completable) {

    companion object {
        private val logger = LoggerFactory.getLogger(TransactionCompletable::class.java)

        private const val SAVE_POINT_PREFIX = "ORMLITE"

        private val savePointCounter = AtomicInteger()
    }

    private var hasSavePoint = false

    private var savePoint: Savepoint? = null

    @Throws(SQLException::class)
    override fun onSubscribe() {
        super.onSubscribe()
        if (savedConnection || connectionSource.databaseType.isNestedSavePointsSupported) {
            savePoint = connection!!.setSavePoint(SAVE_POINT_PREFIX + savePointCounter.incrementAndGet())
            if (savePoint == null) {
                logger.debug("started savePoint transaction")
            } else {
                logger.debug("started savePoint transaction {}", savePoint?.savepointName)
            }
            hasSavePoint = true
        }
    }

    @Throws(SQLException::class)
    override fun onComplete() {
        super.onComplete()
        if (hasSavePoint) {
            commit()
        }
    }

    @Throws(SQLException::class)
    override fun onError() {
        super.onError()
        if (hasSavePoint) {
            rollBack()
        }
    }

    @Throws(SQLException::class)
    private fun commit() {
        connection?.let { connection ->
            val name = savePoint?.savepointName
            connection.commit(savePoint)
            if (name == null) {
                logger.debug("committed savePoint transaction")
            } else {
                logger.debug("committed savePoint transaction {}", name)
            }
        }
    }

    @Throws(SQLException::class)
    private fun rollBack() {
        connection?.let { connection ->
            val name = savePoint?.savepointName
            connection.rollback(savePoint)
            if (name == null) {
                logger.debug("rolled back savePoint transaction")
            } else {
                logger.debug("rolled back savePoint transaction {}", name)
            }
        }
    }
}
