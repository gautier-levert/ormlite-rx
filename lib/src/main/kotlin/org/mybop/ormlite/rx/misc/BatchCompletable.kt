package org.mybop.ormlite.rx.misc

import com.j256.ormlite.logger.LoggerFactory
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import java.sql.SQLException

/**
 * Wrapper around completable to subscribe in batch (auto-commit disabled).
 *
 * Must be used by calling [RxBatchManager.callInBatch][org.mybop.ormlite.rx.misc.RxBatchManager.callInBatch]
 */
internal open class BatchCompletable(
        private val connectionSource: ConnectionSource,
        private val tableName: String?,
        completable: Completable
) : Completable() {

    companion object {
        private val logger = LoggerFactory.getLogger(BatchCompletable::class.java)
    }

    val source: Completable = completable
            .doOnSubscribe { onSubscribe() }
            .doOnComplete { onComplete() }
            .doOnError { onError() }
            .doFinally { finally() }

    protected var connection: DatabaseConnection? = null

    protected var savedConnection = false

    private var restoreAutoCommit = false

    final override fun subscribeActual(s: CompletableObserver) {
        source.subscribe(s)
    }

    @Throws(SQLException::class)
    protected open fun onSubscribe() {
        // open a new connection and save it as default for every next operations
        connection = connectionSource.getReadWriteConnection(tableName)
        savedConnection = connectionSource.saveSpecialConnection(connection)

        if (connection!!.isAutoCommitSupported && connection!!.isAutoCommit) {
            // disable auto-commit mode if supported and enabled at start
            connection!!.isAutoCommit = false
            restoreAutoCommit = true
            logger.debug("had to set auto-commit to false")
        }
    }

    @Throws(SQLException::class)
    protected open fun onComplete() {
    }

    @Throws(SQLException::class)
    protected open fun onError() {
    }

    @Throws(SQLException::class)
    protected open fun finally() {
        connection?.let {
            if (restoreAutoCommit) {
                // try to restore if we are in auto-commit mode
                it.isAutoCommit = true
                logger.debug("restored auto-commit to true")
            }
            if (savedConnection) {
                // remove connection as default for every operations
                connectionSource.clearSpecialConnection(it)
            }
            // release the connection
            connectionSource.releaseConnection(it)
        }
    }
}
