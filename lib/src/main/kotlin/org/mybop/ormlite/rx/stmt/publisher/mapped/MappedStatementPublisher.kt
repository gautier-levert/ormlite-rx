package org.mybop.ormlite.rx.stmt.publisher.mapped

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.stmt.mapped.BaseMappedStatement
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import io.reactivex.internal.subscriptions.EmptySubscription
import org.reactivestreams.Publisher
import org.reactivestreams.Subscriber
import java.util.concurrent.atomic.AtomicInteger

/**
 * Abstract publisher that will execute the mapped statement and emit the result to
 * the subscriber. As mapped statement are insert/update/delete operation, you can
 * only subscribe once to the publisher. Calling [subscribe] a second time will throw
 * an [IllegalStateException].
 *
 * @param T The class that the code will be operating on.
 * @param ID The class of the ID column associated with the class.
 * @param R Type of the result of the mapped statement
 * @param Q Type of the mapped statement. Must be a subclass of [BaseMappedStatement].
 * @param S Type of the subscription that will emit the result of the mapped statement.
 *
 *  Must be a subclass of [MappedStatementSubscription].
 * @param connectionSource An open [ConnectionSource] to the database.
 * @param tableName Table on which the mapped statement will be executed.
 * @param mappedStatement Mapped statement to execute when the publisher subscribes and requests at least one result.
 * @param objectCache Optional [ObjectCache].
 *
 * @see MappedStatementSubscription
 */
abstract class MappedStatementPublisher<
        T, ID, R,
        in Q : BaseMappedStatement<T, ID>,
        out S : MappedStatementSubscription<T, ID, R, Q>>(
        private val connectionSource: ConnectionSource,
        private val tableName: String,
        private val mappedStatement: Q,
        private val objectCache: ObjectCache?
) : Publisher<R> {

    private val atomicSubscriptionCount = AtomicInteger()

    override fun subscribe(subscriber: Subscriber<in R>) {
        val subscriptionCount = atomicSubscriptionCount.incrementAndGet()
        if (subscriptionCount > 1) {
            throw IllegalStateException("Multiple subscription on create/update/delete is not allowed.")
        }

        var connection: DatabaseConnection? = null
        try {
            connection = connectionSource.getReadWriteConnection(tableName)
            val subscription = createSubscription(subscriber, connectionSource, connection,
                    mappedStatement, objectCache)
            subscriber.onSubscribe(subscription)
        } catch (t: Throwable) {
            EmptySubscription.error(t, subscriber)
            connection?.closeQuietly()
        }
    }

    /**
     * Create the subscription that will emit the result of the mapped statement to the [Subscriber].
     *
     * @param subscriber [Subscriber] that will receive the result of the mapped statement.
     * @param connectionSource [ConnectionSource] that have provided the connection.
     * @param connection Open database connection on which the statement will be executed.
     * @param mappedStatement Mapped statement to execute when the publisher subscribes and requests.
     * @param objectCache Optional [ObjectCache].
     * @return A [MappedStatementSubscription][org.mybop.ormlite.rx.stmt.publisher.mapped.MappedStatementSubscription] that will emit the result of the statement to the [Subscriber].
     */
    abstract fun createSubscription(
            subscriber: Subscriber<in R>,
            connectionSource: ConnectionSource,
            connection: DatabaseConnection,
            mappedStatement: Q,
            objectCache: ObjectCache?): S
}
