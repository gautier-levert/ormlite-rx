package org.mybop.ormlite.rx.stmt.publisher.compiled

import com.j256.ormlite.support.CompiledStatement
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import io.reactivex.internal.subscriptions.EmptySubscription
import org.mybop.ormlite.rx.support.releaseConnectionQuietly
import org.reactivestreams.Publisher
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import java.sql.SQLException

/**
 * Abstract publisher that publishes all the results of the compiled statement returned by the [getCompiledStatement] function
 * to the subscriber.
 *
 * Connection with the database will only be initiated when the publisher subscribes.
 * It will be automatically closed when all the results are pushed or on unsubscription.
 *
 * @param R The class that the result will be mapped to.
 * @param connectionSource An open [ConnectionSource] to the database.
 * @param tableName Name of the table from which we will read.
 * @param readOnlyOperation true if we can open a read-only connection to execute the statement, false otherwise.
 */
abstract class CompiledStatementPublisher<R>(
        private val connectionSource: ConnectionSource,
        private val tableName: String?,
        private val readOnlyOperation: Boolean
) : Publisher<R> {

    override fun subscribe(subscriber: Subscriber<in R>) {
        var databaseConnection: DatabaseConnection? = null
        var compiledStatement: CompiledStatement? = null
        try {
            databaseConnection = if (readOnlyOperation) connectionSource.getReadOnlyConnection(tableName)
            else connectionSource.getReadWriteConnection(tableName)

            compiledStatement = getCompiledStatement(databaseConnection)
            val subscription = createSubscription(subscriber, databaseConnection, compiledStatement)
            subscriber.onSubscribe(subscription)
        } catch (e: SQLException) {
            connectionSource.releaseConnectionQuietly(databaseConnection)
            compiledStatement?.closeQuietly()
            EmptySubscription.error(e, subscriber)
        }
    }

    /**
     * Returns the [CompiledStatement] that will be executed by the subscription when requested by the subscriber.
     */
    protected abstract fun getCompiledStatement(databaseConnection: DatabaseConnection): CompiledStatement

    /**
     * Create the subscription that will publish all the results of the [CompiledStatement] to the subscriber.
     *
     * @param subscriber [Subscriber] which will receive the results.
     * @param databaseConnection An open [DatabaseConnection] on which the statement will be executed.
     * @param compiledStatement Statement to execute.
     */
    protected abstract fun createSubscription(
            subscriber: Subscriber<in R>,
            databaseConnection: DatabaseConnection,
            compiledStatement: CompiledStatement): Subscription
}
