package org.mybop.ormlite.rx.stmt.publisher.compiled.select

import com.j256.ormlite.stmt.GenericRowMapper
import com.j256.ormlite.support.CompiledStatement
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import com.j256.ormlite.support.DatabaseResults
import com.nhaarman.mockito_kotlin.*
import org.junit.Assert.*
import org.junit.Test
import org.reactivestreams.Subscriber
import java.sql.SQLException


class CompiledSelectSubscriptionTest {

    @Test
    fun requestNext() {
        val subscriber = mock<Subscriber<Int>>()
        val connectionSource = mock<ConnectionSource>()
        val connection = mock<DatabaseConnection>()
        val compiledStatement = mock<CompiledStatement>()
        val databaseResults = mock<DatabaseResults> {
            on { first() } doReturn true
            on { next() } doReturn listOf(true, false)
        }
        val rowMapper = mock<GenericRowMapper<Int>> {
            on { mapRow(databaseResults) } doReturn listOf(1, 2)
        }

        val subscription = CompiledSelectSubscription(
                subscriber,
                connectionSource,
                connection,
                compiledStatement,
                databaseResults,
                rowMapper
        )

        subscription.request(4)

        verify(subscriber).onNext(1)
        verify(subscriber).onNext(2)
        verify(subscriber).onComplete()

        verify(connectionSource).releaseConnection(connection)
        verify(compiledStatement).closeQuietly()
    }

    @Test
    fun requestNext_notFullyConsumed() {
        val subscriber = mock<Subscriber<Int>>()
        val connectionSource = mock<ConnectionSource>()
        val connection = mock<DatabaseConnection>()
        val compiledStatement = mock<CompiledStatement>()
        val databaseResults = mock<DatabaseResults> {
            on { first() } doReturn true
            on { next() } doReturn listOf(true, false)
        }
        val rowMapper = mock<GenericRowMapper<Int>> {
            on { mapRow(databaseResults) } doReturn listOf(1, 2)
        }

        val subscription = CompiledSelectSubscription(
                subscriber,
                connectionSource,
                connection,
                mock(),
                databaseResults,
                rowMapper
        )

        subscription.request(1)

        verify(subscriber).onNext(1)
        verify(subscriber, never()).onComplete()

        verify(connectionSource, never()).releaseConnection(connection)
        verify(compiledStatement, never()).closeQuietly()
    }

    @Test
    fun requestNext_canceled() {
        val subscriber = mock<Subscriber<Int>>()
        val connectionSource = mock<ConnectionSource>()
        val connection = mock<DatabaseConnection>()
        val compiledStatement = mock<CompiledStatement>()
        val databaseResults = mock<DatabaseResults> {
            on { first() } doReturn true
            on { next() } doReturn listOf(true, false)
        }
        val rowMapper = mock<GenericRowMapper<Int>> {
            on { mapRow(databaseResults) } doReturn listOf(1, 2)
        }

        val subscription = CompiledSelectSubscription(
                subscriber,
                connectionSource,
                connection,
                compiledStatement,
                databaseResults,
                rowMapper
        )

        subscription.request(1)
        verify(subscriber).onNext(1)

        subscription.cancel()
        verify(connectionSource).releaseConnection(connection)
        verify(compiledStatement).closeQuietly()

        subscription.request(1)
        verify(subscriber, never()).onNext(2)
    }

    @Test
    fun requestNext_forwardException() {
        val subscriber = mock<Subscriber<Any>>()
        val databaseResults = mock<DatabaseResults> {
            on { first() } doThrow SQLException("A random SQLException")
        }

        val subscription = CompiledSelectSubscription(
                subscriber,
                mock(),
                mock(),
                mock(),
                databaseResults,
                mock()
        )

        subscription.request(1)
        verify(subscriber).onError(any())
    }

    @Test
    fun moveToNextRow() {
        val connectionSource = mock<ConnectionSource>()
        val connection = mock<DatabaseConnection>()
        val compiledStatement = mock<CompiledStatement>()
        val databaseResults = mock<DatabaseResults> {
            on { first() } doReturn true
            on { next() } doReturn listOf(true, false)
        }

        val subscription = CompiledSelectSubscription(
                mock<Subscriber<Any>>(),
                connectionSource,
                connection,
                compiledStatement,
                databaseResults,
                mock()
        )

        assertTrue(subscription.moveToNextRow())
        verify(databaseResults).first()
        verify(connectionSource, never()).releaseConnection(connection)
        verify(compiledStatement, never()).closeQuietly()

        assertTrue(subscription.moveToNextRow())
        verify(databaseResults).next()
        verify(connectionSource, never()).releaseConnection(connection)
        verify(compiledStatement, never()).closeQuietly()

        assertFalse(subscription.moveToNextRow())
        verify(connectionSource).releaseConnection(connection)
        verify(compiledStatement).closeQuietly()
    }

    @Test
    fun moveToNextRow_emptyResults() {
        val connectionSource = mock<ConnectionSource>()
        val connection = mock<DatabaseConnection>()
        val compiledStatement = mock<CompiledStatement>()
        val databaseResults = mock<DatabaseResults> {
            on { first() } doReturn false
        }

        val subscription = CompiledSelectSubscription(
                mock<Subscriber<Any>>(),
                connectionSource,
                connection,
                compiledStatement,
                databaseResults,
                mock()
        )

        assertFalse(subscription.moveToNextRow())
        verify(connectionSource).releaseConnection(connection)
        verify(compiledStatement).closeQuietly()
    }

    @Test
    fun nextRowAsObject() {
        val databaseResults = mock<DatabaseResults> {
            on { first() } doReturn true
            on { next() } doReturn listOf(true, false)
        }

        val rowMapper = mock<GenericRowMapper<Int>> {
            on { mapRow(databaseResults) } doReturn listOf(1, 2, 3)
        }

        val subscription = CompiledSelectSubscription(
                mock<Subscriber<Any>>(),
                mock(),
                mock(),
                mock(),
                databaseResults,
                rowMapper
        )

        assertEquals(1, subscription.nextRowAsObject())
        verify(databaseResults).first()

        assertEquals(2, subscription.nextRowAsObject())
        verify(databaseResults).next()

        assertNull(subscription.nextRowAsObject())
    }

    @Test
    fun closeConnectionQuietly() {
        val statement = mock<CompiledStatement>()
        val connectionSource = mock<ConnectionSource>()
        val connection = mock<DatabaseConnection>()

        val subscription = CompiledSelectSubscription(
                mock<Subscriber<Any>>(),
                connectionSource,
                connection,
                statement,
                mock(),
                mock()
        )

        subscription.closeConnectionQuietly()
        verify(statement).closeQuietly()
        verify(connectionSource).releaseConnection(connection)

        subscription.closeConnectionQuietly()
        verify(statement).closeQuietly()
        verify(connectionSource).releaseConnection(connection)
    }
}
