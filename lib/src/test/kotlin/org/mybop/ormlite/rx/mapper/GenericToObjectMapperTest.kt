package org.mybop.ormlite.rx.mapper

import com.j256.ormlite.dao.RawRowObjectMapper
import com.j256.ormlite.field.DataType
import com.j256.ormlite.support.DatabaseResults
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.Test

class GenericToObjectMapperTest {

    @Test
    fun mapRow() {
        val columnNames = arrayOf("column0", "column1", "column2")
        val dataTypes = arrayOf(DataType.STRING, DataType.LONG, DataType.DOUBLE)
        val result = mock<DatabaseResults> {
            on { columnCount } doReturn 3
            on { it.columnNames } doReturn columnNames
            on { getString(0) } doReturn "test"
            on { getLong(1) } doReturn 1L
            on { getDouble(2) } doReturn 42.0
        }
        val rowMapper = mock<RawRowObjectMapper<Any>>()

        val mapper = GenericToObjectMapper(dataTypes, rowMapper)
        mapper.mapRow(result)

        verify(rowMapper).mapRow(columnNames, dataTypes, arrayOf("test", 1L, 42.0))
    }
}
