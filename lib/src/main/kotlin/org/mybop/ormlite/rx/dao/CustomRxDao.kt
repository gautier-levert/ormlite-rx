package org.mybop.ormlite.rx.dao

import kotlin.reflect.KClass

/**
 * Annotation used to define a custom [RxDao] for an entity.
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class CustomRxDao(
        /**
         * Class that will be used as [RxDao]
         *
         * **NOTE:** this class must be an instance of [BaseRxDaoImpl]
         */
        val rxDaoClass: KClass<out BaseRxDaoImpl<*, *>>
)
