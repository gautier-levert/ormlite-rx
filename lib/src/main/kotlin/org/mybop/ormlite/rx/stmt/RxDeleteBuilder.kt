package org.mybop.ormlite.rx.stmt

import com.j256.ormlite.dao.Dao
import com.j256.ormlite.stmt.DeleteBuilder
import com.j256.ormlite.stmt.PreparedDelete
import io.reactivex.Completable
import io.reactivex.Single
import org.mybop.ormlite.rx.dao.RxDao
import org.mybop.ormlite.rx.stmt.where.RxDeleteWhere

/**
 * Assists in building sql DELETE statements for a particular table in a particular database.
 *
 * @param <T>
 *            The class that the code will be operating on.
 * @param <ID>
 *            The class of the ID column associated with the class. The T class does not require an ID field. The class
 *            needs an ID parameter however so you can use Void or Object to satisfy the compiler.
 * @see com.j256.ormlite.stmt.DeleteBuilder
 */
class RxDeleteBuilder<T, ID>(
        private val deleteBuilder: DeleteBuilder<T, ID>,
        private val rxDao: RxDao<T, ID>
) : RxStatementBuilder<T, ID, RxDeleteWhere<T, ID>>(deleteBuilder) {

    override fun where(): RxDeleteWhere<T, ID> = RxDeleteWhere(deleteBuilder.where(), this)

    /**
     * Build and return a prepared delete that can be used by [Dao.delete] method. If you change
     * the where or make other calls you will need to re-call this method to re-prepare the statement for execution.
     */
    override fun prepare(): Single<PreparedDelete<T>> = Single.fromCallable { deleteBuilder.prepare() }

    /**
     * A short cut to [Dao.delete].
     */
    fun delete(): Completable = prepare()
            .flatMapCompletable { rxDao.delete(it) }
}
