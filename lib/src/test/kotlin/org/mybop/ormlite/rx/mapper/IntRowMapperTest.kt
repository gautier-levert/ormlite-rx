package org.mybop.ormlite.rx.mapper

import com.j256.ormlite.support.DatabaseResults
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import org.junit.Assert.assertEquals
import org.junit.Test

class IntRowMapperTest {

    @Test
    fun mapRow() {
        val results = mock<DatabaseResults> {
            on { it.getInt(0) } doReturn 42
        }

        val mapper = IntRowMapper
        assertEquals(42, mapper.mapRow(results))
    }
}
