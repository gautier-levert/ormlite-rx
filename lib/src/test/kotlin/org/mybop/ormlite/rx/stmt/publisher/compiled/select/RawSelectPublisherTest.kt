package org.mybop.ormlite.rx.stmt.publisher.compiled.select

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.field.SqlType
import com.j256.ormlite.stmt.GenericRowMapper
import com.j256.ormlite.stmt.StatementBuilder
import com.j256.ormlite.support.CompiledStatement
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import com.j256.ormlite.support.DatabaseResults
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.Test
import org.reactivestreams.Subscriber

class RawSelectPublisherTest {

    @Test
    fun subscribe() {
        val tableName = "test"
        val query = "SELECT * FROM test WHERE value = ? AND id = ?"
        val arguments = arrayOf("value", "testId")
        val objectCache = mock<ObjectCache>()
        val compiledStatement = mock<CompiledStatement> {
            on { runQuery(objectCache) } doReturn mock<DatabaseResults>()
        }
        val connection = mock<DatabaseConnection> {
            on {
                compileStatement(query, StatementBuilder.StatementType.SELECT, emptyArray(),
                        DatabaseConnection.DEFAULT_RESULT_FLAGS, false)
            } doReturn compiledStatement
        }
        val connectionSource = mock<ConnectionSource> {
            on { getReadOnlyConnection(tableName) } doReturn connection
        }
        val subscriber = mock<Subscriber<in Any>>()
        val maxRows = 1

        val publisher = RawSelectPublisher(
                connectionSource,
                tableName,
                query,
                arguments,
                mock<GenericRowMapper<Any>>(),
                objectCache,
                maxRows
        )

        publisher.subscribe(subscriber)

        verify(compiledStatement).setObject(0, arguments[0], SqlType.STRING)
        verify(compiledStatement).setObject(1, arguments[1], SqlType.STRING)
        verify(compiledStatement).setMaxRows(maxRows)

        verify(compiledStatement).runQuery(objectCache)

        verify(subscriber).onSubscribe(any<CompiledSelectSubscription<Any>>())
    }
}
