package org.mybop.ormlite.rx.misc

import com.j256.ormlite.jdbc.JdbcConnectionSource
import com.j256.ormlite.table.TableUtils
import io.reactivex.Completable
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mybop.ormlite.rx.dao.RxDao
import org.mybop.ormlite.rx.dao.RxDaoManager
import org.mybop.ormlite.rx.model.TestModel

class RxTransactionManagerIntegrationTest {

    private lateinit var connectionSource: JdbcConnectionSource

    @Before
    fun setUp() {
        connectionSource = JdbcConnectionSource("jdbc:h2:mem:test;DB_CLOSE_DELAY=0")

        TableUtils.createTable(connectionSource, TestModel::class.java)
    }

    @After
    fun tearDown() {
        connectionSource.closeQuietly()
    }

    @Test
    fun callInTransaction_commit() {
        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource, TestModel::class.java)
        val transactionManager = RxTransactionManager(connectionSource)

        val insert = testModelRxDao.create(TestModel(1))
                .ignoreElement()

        transactionManager.callInTransaction(insert)
                .blockingAwait()

        Assertions.assertThat(testModelRxDao.countOf().blockingGet()).isEqualTo(1)
    }

    @Test
    fun callInTransaction_error() {
        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource, TestModel::class.java)
        val transactionManager = RxTransactionManager(connectionSource)

        val insert = testModelRxDao.create(TestModel(1))
                .flatMapCompletable { Completable.error(IllegalStateException("this is an error")) }

        val transaction = transactionManager.callInTransaction(insert).test()
        assertThat(transaction.errorCount()).isNotEqualTo(0)

        assertThat(testModelRxDao.countOf().blockingGet()).isEqualTo(0)
    }

    @Test
    fun callInTransaction_batch_error() {
        val testModelRxDao: RxDao<TestModel, Long> = RxDaoManager.createDao(connectionSource, TestModel::class.java)
        val transactionManager = RxTransactionManager(connectionSource)

        val insert = testModelRxDao.create(listOf(TestModel(1), TestModel(2), TestModel(3)))
                .andThen(Completable.error(IllegalStateException("this is an error")))

        val transaction = transactionManager.callInTransaction(insert).test()
        assertThat(transaction.errorCount()).isNotEqualTo(0)

        assertThat(testModelRxDao.countOf().blockingGet()).isEqualTo(0)
    }
}
