package org.mybop.ormlite.rx.support

import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import java.sql.SQLException

fun ConnectionSource.releaseConnectionQuietly(connection: DatabaseConnection?) {
    if (connection != null) {
        try {
            releaseConnection(connection)
        } catch (e: SQLException) {
            // Swallow exception
        }
    }
}
