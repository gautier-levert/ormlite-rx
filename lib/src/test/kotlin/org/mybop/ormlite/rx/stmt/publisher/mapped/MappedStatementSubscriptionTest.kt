package org.mybop.ormlite.rx.stmt.publisher.mapped

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.stmt.mapped.BaseMappedStatement
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.spy
import com.nhaarman.mockito_kotlin.verify
import org.junit.Assert.assertTrue
import org.junit.Test
import org.reactivestreams.Subscriber

class MappedStatementSubscriptionTest {

    @Test
    fun request() {
        val subscriber = mock<Subscriber<Int>>()
        val connectionSource = mock<ConnectionSource>()
        val connection = mock<DatabaseConnection>()
        val mappedStatement = mock<BaseMappedStatement<Any, Any>>()
        val objectCache = mock<ObjectCache>()

        val subscription = spy(StubMappedStatementSubscription(
                subscriber,
                connectionSource,
                connection,
                mappedStatement,
                objectCache
        ))

        subscription.request(1)

        verify(subscription).execute(mappedStatement, connection, objectCache)
        verify(subscriber).onNext(1)
        verify(subscriber).onComplete()
        verify(connectionSource).releaseConnection(connection)
        assertTrue(subscription.closed)
    }

    private class StubMappedStatementSubscription<T, ID>(
            subscriber: Subscriber<Int>,
            connectionSource: ConnectionSource,
            connection: DatabaseConnection,
            mappedStatement: BaseMappedStatement<T, ID>,
            objectCache: ObjectCache?
    ) : MappedStatementSubscription<T, ID, Int, BaseMappedStatement<T, ID>>(
            subscriber, connectionSource, connection, mappedStatement, objectCache
    ) {
        override fun execute(mappedStatement: BaseMappedStatement<T, ID>, connection: DatabaseConnection, objectCache: ObjectCache?): Int
                = 1
    }
}
