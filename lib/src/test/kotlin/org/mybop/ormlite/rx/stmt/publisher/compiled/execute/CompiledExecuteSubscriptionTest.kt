package org.mybop.ormlite.rx.stmt.publisher.compiled.execute

import com.j256.ormlite.support.CompiledStatement
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import org.junit.Test
import org.reactivestreams.Subscriber

class CompiledExecuteSubscriptionTest {

    @Test
    fun request_execute() {
        val result = 42
        val subscriber = mock<Subscriber<in Int>>()
        val connectionSource = mock<ConnectionSource>()
        val connection = mock<DatabaseConnection>()
        val compiledStatement = mock<CompiledStatement> {
            on { runExecute() } doReturn result
        }

        val subscription = CompiledExecuteSubscription(
                subscriber,
                connectionSource,
                connection,
                compiledStatement,
                false
        )

        subscription.request(1)

        verify(compiledStatement).runExecute()
        verify(compiledStatement, never()).runUpdate()
        verify(subscriber).onNext(result)
        verify(subscriber).onComplete()
        verify(compiledStatement).closeQuietly()
        verify(connectionSource).releaseConnection(connection)
    }

    @Test
    fun request_update() {
        val result = 42
        val subscriber = mock<Subscriber<in Int>>()
        val connectionSource = mock<ConnectionSource>()
        val connection = mock<DatabaseConnection>()
        val compiledStatement = mock<CompiledStatement> {
            on { runUpdate() } doReturn result
        }

        val subscription = CompiledExecuteSubscription(
                subscriber,
                connectionSource,
                connection,
                compiledStatement,
                true
        )

        subscription.request(1)

        verify(compiledStatement).runUpdate()
        verify(compiledStatement, never()).runExecute()
        verify(subscriber).onNext(result)
        verify(subscriber).onComplete()
        verify(compiledStatement).closeQuietly()
        verify(connectionSource).releaseConnection(connection)
    }
}
