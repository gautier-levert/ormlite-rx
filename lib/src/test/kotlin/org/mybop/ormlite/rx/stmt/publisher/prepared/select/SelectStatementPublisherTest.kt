package org.mybop.ormlite.rx.stmt.publisher.prepared.select

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.stmt.GenericRowMapper
import com.j256.ormlite.stmt.PreparedQuery
import com.j256.ormlite.stmt.StatementBuilder
import com.j256.ormlite.support.CompiledStatement
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import com.j256.ormlite.support.DatabaseResults
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.Test
import org.reactivestreams.Subscriber

class SelectStatementPublisherTest {

    @Test
    fun subscribe() {
        val connection = mock<DatabaseConnection>()
        val connectionSource = mock<ConnectionSource> {
            on { getReadOnlyConnection("test") } doReturn connection
        }
        val compiledStatement = mock<CompiledStatement> {
            on { runQuery(any()) } doReturn mock<DatabaseResults>()
        }
        val preparedQuery = mock<PreparedQuery<Any>> {
            on { type } doReturn StatementBuilder.StatementType.SELECT
            on { compile(any(), any()) } doReturn compiledStatement
        }

        val publisher = SelectStatementPublisher<Any, Any>(
                connectionSource,
                "test",
                preparedQuery,
                mock(),
                mock()
        )
        publisher.subscribe(mock<Subscriber<Any>>())
        verify(preparedQuery).compile(any(), any())
    }
}
