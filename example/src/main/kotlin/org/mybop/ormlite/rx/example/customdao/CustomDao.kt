package org.mybop.ormlite.rx.example.customdao

import com.j256.ormlite.jdbc.JdbcConnectionSource
import com.j256.ormlite.table.TableUtils
import org.mybop.ormlite.rx.dao.RxDaoManager

const val DATABASE_URL = "jdbc:h2:mem:account"

fun main(vararg args: String) {
    JdbcConnectionSource(DATABASE_URL).use { connectionSource ->
        val accountDao = RxDaoManager.createDao<Account, Int>(connectionSource, Account::class.java) as AccountDao

        // if you need to create the table
        TableUtils.createTable(connectionSource, Account::class.java)

        // create an instance of Account
        val name = "Jim Coakley"
        val account = Account(name = name)

        // persist the account object to the database
        accountDao.create(account).subscribe()

        // assign a password
        account.password = "_secret"

        // update the database after changing the object
        accountDao.update(account).subscribe()

        accountDao.findAllActive().subscribe {
            println(it)
        }

        accountDao.deactivate(account).subscribe()

        accountDao.findAllActive().subscribe {
            println(it)
        }

        accountDao.activate(account).subscribe()

        accountDao.findAllActive().subscribe {
            println(it)
        }
    }
}
