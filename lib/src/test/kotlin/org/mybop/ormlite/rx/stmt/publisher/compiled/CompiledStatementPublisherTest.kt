package org.mybop.ormlite.rx.stmt.publisher.compiled

import com.j256.ormlite.support.CompiledStatement
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Flowable
import org.junit.Test
import org.mybop.ormlite.rx.model.TestModel
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import java.sql.SQLException

class CompiledStatementPublisherTest {

    @Test
    fun subscribe() {
        val tableName = "test"
        val subscriber = mock<Subscriber<Any>>()
        val connection = mock<DatabaseConnection>()
        val connectionSource = mock<ConnectionSource> {
            on { getReadWriteConnection(tableName) } doReturn connection
        }
        val compiledStatement = mock<CompiledStatement>()
        val subscription = mock<Subscription>()

        val publisher = spy(MockCompiledSelectPublisher<Any>(
                connectionSource,
                tableName,
                false,
                compiledStatement,
                subscription
        ))

        publisher.subscribe(subscriber)

        verify(connectionSource).getReadWriteConnection(tableName)
        verify(publisher).getCompiledStatement(connection)
        verify(publisher).createSubscription(subscriber, connection, compiledStatement)
        verify(subscriber).onSubscribe(subscription)
    }

    @Test
    fun subscribe_readOnly() {
        val tableName = "test"
        val connection = mock<DatabaseConnection>()
        val connectionSource = mock<ConnectionSource> {
            on { getReadOnlyConnection(tableName) } doReturn connection
        }
        val compiledStatement = mock<CompiledStatement>()
        val subscription = mock<Subscription>()

        val publisher = spy(MockCompiledSelectPublisher<Any>(
                connectionSource,
                tableName,
                true,
                compiledStatement,
                subscription
        ))

        publisher.subscribe(mock())

        verify(connectionSource).getReadOnlyConnection(tableName)
    }

    @Test
    fun subscribe_connectionOnlyCreatedWhenSubscribe() {
        val connection = mock<DatabaseConnection>()
        val connectionSource = mock<ConnectionSource> {
            on { getReadOnlyConnection(any()) } doReturn connection
        }
        val compiledStatement = mock<CompiledStatement>()

        Flowable.fromPublisher(MockCompiledSelectPublisher<TestModel>(
                connectionSource,
                "test",
                true,
                compiledStatement,
                mock()
        ))
                .filter { it.id!! == 0L }

        verify(connectionSource, never()).getReadOnlyConnection(any())

        Flowable.fromPublisher(MockCompiledSelectPublisher<TestModel>(
                connectionSource,
                "test",
                true,
                compiledStatement,
                mock()
        ))
                .filter { it.id!! == 0L }
                .subscribe()

        verify(connectionSource).getReadOnlyConnection(any())
    }

    @Test
    fun subscribe_failingQuery() {
        val subscriber = mock<Subscriber<Any>>()
        val connection = mock<DatabaseConnection>()
        val connectionSource = mock<ConnectionSource> {
            on { getReadOnlyConnection(any()) } doReturn connection
        }
        val compiledStatement = mock<CompiledStatement>()

        val publisher = FailingCompiledSelectPublisher<Any>(
                connectionSource,
                "test",
                compiledStatement
        )

        publisher.subscribe(subscriber)

        verify(subscriber).onSubscribe(any())
        verify(subscriber).onError(any<SQLException>())
        verify(subscriber, never()).onComplete()
        verify(connectionSource).releaseConnection(connection)
        verify(compiledStatement).closeQuietly()
    }

    private class MockCompiledSelectPublisher<R>(
            connectionSource: ConnectionSource,
            tableName: String,
            readOnlyOperation: Boolean,
            private val compiledStatement: CompiledStatement,
            private val subscription: Subscription
    ) : CompiledStatementPublisher<R>(connectionSource, tableName, readOnlyOperation) {
        public override fun getCompiledStatement(databaseConnection: DatabaseConnection): CompiledStatement = compiledStatement
        public override fun createSubscription(
                subscriber: Subscriber<in R>,
                databaseConnection: DatabaseConnection,
                compiledStatement: CompiledStatement): Subscription = subscription
    }

    private class FailingCompiledSelectPublisher<R>(
            connectionSource: ConnectionSource,
            tableName: String,
            private val compiledStatement: CompiledStatement
    ) : CompiledStatementPublisher<R>(connectionSource, tableName, true) {
        public override fun getCompiledStatement(databaseConnection: DatabaseConnection): CompiledStatement = compiledStatement
        public override fun createSubscription(
                subscriber: Subscriber<in R>,
                databaseConnection: DatabaseConnection,
                compiledStatement: CompiledStatement): Subscription = throw SQLException()
    }
}
