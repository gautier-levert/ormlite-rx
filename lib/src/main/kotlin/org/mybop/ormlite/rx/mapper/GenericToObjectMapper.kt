package org.mybop.ormlite.rx.mapper

import com.j256.ormlite.dao.RawRowObjectMapper
import com.j256.ormlite.field.DataPersister
import com.j256.ormlite.field.DataType
import com.j256.ormlite.stmt.GenericRowMapper
import com.j256.ormlite.support.DatabaseResults
import java.sql.SQLException

/**
 * This is an implementation of [GenericRowMapper] used by [org.mybop.ormlite.rx.dao.BaseRxDaoImpl.queryRaw]
 * to support [DataType] mapping.
 *
 * **NOTE:** This mapper cannot be used with multiple requests cause it keep column names in cache
 */
internal class GenericToObjectMapper<R>(
        private val dataTypes: Array<DataType>,
        private val rowMapper: RawRowObjectMapper<R>
) : GenericRowMapper<R> {

    private val persisters: Array<DataPersister> = dataTypes.map { it.dataPersister }.toTypedArray()

    private var columnNames: Array<String>? = null

    @Throws(SQLException::class)
    override fun mapRow(results: DatabaseResults): R {

        val objectResults = (0 until results.columnCount)
                .map {
                    if (it >= persisters.size) {
                        null
                    } else {
                        persisters[it].resultToJava(null, results, it)
                    }
                }
                .toTypedArray()

        return rowMapper.mapRow(getColumnNames(results), dataTypes, objectResults)
    }

    @Throws(SQLException::class)
    private fun getColumnNames(results: DatabaseResults): Array<String> = synchronized(this) {
        if (columnNames != null) {
            return columnNames!!
        }
        columnNames = results.columnNames
        return columnNames!!
    }
}
