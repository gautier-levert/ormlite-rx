package org.mybop.ormlite.rx.example.customdao

import com.j256.ormlite.dao.BaseDaoImpl
import org.mybop.ormlite.rx.dao.BaseRxDaoImpl

class AccountDao(baseDaoImpl: BaseDaoImpl<Account, Int>) : BaseRxDaoImpl<Account, Int>(baseDaoImpl) {

    fun findAllActive() = queryBuilder()
            .where()
            .eq(Account.ACTIVE_COLUMN_NAME, true)
            .query()

    fun activate(account: Account) = updateBuilder()
            .updateColumnValue(Account.ACTIVE_COLUMN_NAME, true)
            .where()
            .idEq(account.id)
            .update()

    fun deactivate(account: Account) = updateBuilder()
            .updateColumnValue(Account.ACTIVE_COLUMN_NAME, false)
            .where()
            .idEq(account.id)
            .update()
}
