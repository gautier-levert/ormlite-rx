package org.mybop.ormlite.rx.stmt.publisher.prepared.select

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.stmt.GenericRowMapper
import com.j256.ormlite.stmt.PreparedStmt
import com.j256.ormlite.stmt.StatementBuilder
import com.j256.ormlite.support.CompiledStatement
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import org.mybop.ormlite.rx.stmt.publisher.compiled.CompiledStatementPublisher
import org.mybop.ormlite.rx.stmt.publisher.compiled.select.CompiledSelectSubscription
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription

/**
 * Publisher that publishes all the results of the select prepared statement to the subscriber.
 *
 * Connection with the database will only be initiated when the publisher subscribes.
 * It will be automatically closed when all the results are pushed or on unsubscription.
 *
 * @param T The class that the code will be operating on.
 * @param R The class that the result row will be mapped to.
 * @param connectionSource An open [ConnectionSource] to the database.
 * @param tableName Name of the table from which we will read.
 * @param preparedStmt Statement to execute.
 * @param rowMapper [GenericRowMapper] that will convert the rows returned by the database into the object that will be emitted.
 * @param objectCache An optional [ObjectCache].
 * @param maxRows Maximum number of rows to read from the results. null to read all.
 */
class SelectStatementPublisher<T, R>(
        private val connectionSource: ConnectionSource,
        private val tableName: String,
        private val preparedStmt: PreparedStmt<T>,
        private val rowMapper: GenericRowMapper<R>,
        private val objectCache: ObjectCache?,
        private val maxRows: Int? = null
) : CompiledStatementPublisher<R>(connectionSource, tableName, true) {

    companion object {
        val supportedStatementTypes = listOf(
                StatementBuilder.StatementType.SELECT,
                StatementBuilder.StatementType.SELECT_LONG,
                StatementBuilder.StatementType.SELECT_RAW)
    }

    init {
        if (!supportedStatementTypes.contains(preparedStmt.type)) {
            throw IllegalStateException("${SelectStatementPublisher::class.java.simpleName} only supports SELECT queries.")
        }
    }

    override fun getCompiledStatement(databaseConnection: DatabaseConnection): CompiledStatement {
        val compiledStatement = preparedStmt.compile(databaseConnection, preparedStmt.type)
        if (maxRows != null) {
            compiledStatement.setMaxRows(maxRows)
        }
        return compiledStatement
    }

    override fun createSubscription(
            subscriber: Subscriber<in R>,
            databaseConnection: DatabaseConnection,
            compiledStatement: CompiledStatement): Subscription
            = CompiledSelectSubscription(subscriber, connectionSource, databaseConnection, compiledStatement,
            compiledStatement.runQuery(objectCache),
            rowMapper
    )
}
