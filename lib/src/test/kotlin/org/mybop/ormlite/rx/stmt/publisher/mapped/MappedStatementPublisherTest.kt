package org.mybop.ormlite.rx.stmt.publisher.mapped

import com.j256.ormlite.dao.ObjectCache
import com.j256.ormlite.stmt.mapped.BaseMappedStatement
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import com.nhaarman.mockito_kotlin.*
import org.junit.Test
import org.reactivestreams.Subscriber
import java.sql.SQLException

class MappedStatementPublisherTest {

    @Test
    fun subscribe() {
        val subscriber = mock<Subscriber<Int>>()
        val connection = mock<DatabaseConnection>()
        val connectionSource = mock<ConnectionSource> {
            on { getReadWriteConnection("test") } doReturn connection
        }
        val mappedStatement = mock<BaseMappedStatement<Any, Any>>()
        val objectCache = mock<ObjectCache>()
        val subscription = mock<MappedStatementSubscription<Any, Any, Int, BaseMappedStatement<Any, Any>>>()
        val abstractMethods = mock<MappedStatementPublisherAbstractMethods<Any, Any>> {
            on { createSubscription(subscriber, connectionSource, connection, mappedStatement, objectCache) } doReturn subscription
        }

        val publisher = StubMappedStatementPublisher(
                connectionSource,
                "test",
                mappedStatement,
                objectCache,
                abstractMethods
        )

        publisher.subscribe(subscriber)
        verify(abstractMethods).createSubscription(subscriber, connectionSource, connection, mappedStatement, objectCache)
        verify(subscriber).onSubscribe(subscription)
    }

    @Test(expected = IllegalStateException::class)
    fun subscribe_onlyOnce() {
        val publisher = StubMappedStatementPublisher<Any, Any>()

        publisher.subscribe(mock())
        publisher.subscribe(mock())
    }

    @Test
    fun subscribe_connectionThrowsException() {
        val subscriber = mock<Subscriber<Int>>()
        val connectionSource = mock<ConnectionSource> {
            on { getReadWriteConnection("test") } doThrow SQLException()
        }

        val publisher = StubMappedStatementPublisher<Any, Any>(
                connectionSource = connectionSource
        )

        publisher.subscribe(subscriber)
        verify(subscriber).onError(any())
    }

    private class StubMappedStatementPublisher<T, ID>(
            connectionSource: ConnectionSource = mock {
                on { getReadWriteConnection("test") } doReturn mock<DatabaseConnection>()
            },
            tableName: String = "test",
            mappedStatement: BaseMappedStatement<T, ID> = mock(),
            objectCache: ObjectCache? = mock(),
            private val abstractMethods: MappedStatementPublisherAbstractMethods<T, ID> = mock()
    ) : MappedStatementPublisher<T, ID, Int, BaseMappedStatement<T, ID>, MappedStatementSubscription<T, ID, Int, BaseMappedStatement<T, ID>>>(
            connectionSource, tableName, mappedStatement, objectCache
    ) {
        override fun createSubscription(subscriber: Subscriber<in Int>, connectionSource: ConnectionSource, connection: DatabaseConnection, mappedStatement: BaseMappedStatement<T, ID>, objectCache: ObjectCache?): MappedStatementSubscription<T, ID, Int, BaseMappedStatement<T, ID>>
                = abstractMethods.createSubscription(subscriber, connectionSource, connection, mappedStatement, objectCache)
    }

    private interface MappedStatementPublisherAbstractMethods<T, ID> {
        fun createSubscription(subscriber: Subscriber<in Int>, connectionSource: ConnectionSource, connection: DatabaseConnection, mappedStatement: BaseMappedStatement<T, ID>, objectCache: ObjectCache?): MappedStatementSubscription<T, ID, Int, BaseMappedStatement<T, ID>>
    }
}
